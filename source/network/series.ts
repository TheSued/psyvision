import Routes from "../environments/Routes";
import http from "./http";
import * as Utils from "../utils/Series.utils";
import { Searchpage } from "../views/Searchpage";
import { Platform } from "react-native";

export async function getSeries(
  companyId: number,
  step: number,
  categoryId: string,
  slideShow: boolean
) {
  let url = `${Routes.GetSeries}&step=${step}&company=${companyId}`;
  if (categoryId) {
    url = `${url}&cat=${categoryId}`;
  }
  if (slideShow) {
    url = `${url}&last=1`;
  }
  const serieList = Utils.formatSerie((await http.get(url)) || []);
  return serieList;
}

export async function getSerieInfo(serieId: string | number) {
  const url = `${Routes.GetSeries}&collection=${serieId}`;
  const data = await http.get(url);

  const serieInfo = Utils.formatSerie(data);

  return serieInfo;
}

export async function getBundleList(
  companyId: number,
  step: number,
  categoryId: string,
  slideShow: boolean
) {
  let url = `${Routes.GetSeries}&step=${step}&company=${companyId}&parent=1&order=DESC`;
  if (categoryId) {
    url = `${url}&cat=${categoryId}`;
  }
  if (slideShow) {
    url = `${url}&last=1`;
  }
  const bundleList = Utils.formatSerie((await http.get(url)) || []);
  return bundleList;
}

export async function getSerieVideo(
  companyId: number,
  serieId: string,
  step: number
) {
  let url = `${Routes.GetVideo}&step=${step}&company=${companyId}&collection=${serieId}&order=DESC`;
  const serieVideoList = (await http.get(url)) || [];
  return serieVideoList;
}

export async function getBundleChild(id: number, step: number) {
  const url = `${Routes.GetBundleChild}&id=${id}&step=${step}`;
  const childs = await http.get(url);
  return childs;
}

export async function getSerieParent(id: number) {
  const url = `${Routes.GetSerieParent}&id=${id}`;
  const parents = Utils.formatBundle(await http.get(url));
  return parents;
}

export async function searchSeries(
  companyId: number,
  step: number,
  text: string,
  categories: number[]
) {
  let url = `${
    Routes.GetSeries
  }&step=${step}&company=${companyId}&search=${text}&cat=${categories.join()}`;
  const searchResult = Utils.formatSerie((await http.get(url)) || []);
  return searchResult;
}
//campi custom carrello
export async function getCustomCartField() {
  const url = `${Routes.CustCart}`;
  const cartResult = Utils.formatCustCart(
    (await http.getNoAuthCart(url)) || []
  );

  return cartResult;
}
export async function validateIAP(
  productId: any,
  purchaseToken: any,
  packageApp: string,
  id_series: any
) {
  const formdata = new FormData();
  formdata.append("package_app", packageApp);
  formdata.append("product_id", productId);
  formdata.append("purchase_token", purchaseToken);
  formdata.append("id_series", id_series);

  const response = await http.post(
    `https://api.teyuto.tv/v2/payments/${Platform.OS}`,
    formdata
  );
  return response;
}

export async function validateIosIAP(purchaseToken: any, id_series: any) {
  const formdata = new FormData();

  formdata.append("purchase_token", purchaseToken);
  formdata.append("id_series", id_series);

  const response = await http.post(
    `https://api.teyuto.tv/v2/payments/${Platform.OS}`,
    formdata
  );
  return response;
}

// pulire pagamenti e formdata, eventuale key univoca
export async function getCustomCartInput(data: any) {
  const formdata = new FormData();
  formdata.append("country", data.country);
  formdata.append("fullname", data.fullName);
  formdata.append("fiscal_code", data.fiscalCode);
  formdata.append("city", data.city);
  formdata.append("province", data.province);
  formdata.append("cap", data.cap);
  formdata.append("sdi_code_pec", data.pecCode);
  formdata.append("vat_num", data.vatNum);
  formdata.append("phone", data.mobilePhone);
  formdata.append("email_for_invoice", data.emailAdd);
  formdata.append("payment_type", data.paymentType);
  formdata.append("type", data.type);
  formdata.append("id", data.id);

  const response = Utils.formatCustCartInput(
    (await http.post(Routes.DetailPricing, formdata)) || []
  );
  return response;
}
