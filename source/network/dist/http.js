"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function (thisArg, body) {
    var _ = {
        label: 0,
        sent: function () {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: [],
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function () {
          return this;
        }),
      g
    );
    function verb(n) {
      return function (v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y["return"]
                  : op[0]
                  ? y["throw"] || ((t = y["return"]) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
exports.__esModule = true;
exports.deleteToken = exports.getToken = void 0;
var react_native_1 = require("react-native");
var Storage_1 = require("../services/Storage");
var token = "";
function getToken() {
  return __awaiter(this, void 0, void 0, function () {
    return __generator(this, function (_a) {
      switch (_a.label) {
        case 0:
          if (token) {
            return [2 /*return*/, token];
          }
          return [4 /*yield*/, Storage_1["default"].getToken()];
        case 1:
          token = _a.sent();
          return [2 /*return*/, token];
      }
    });
  });
}
exports.getToken = getToken;
function deleteToken() {
  token = "";
}
exports.deleteToken = deleteToken;
var http = {
  login: function (url, formdata) {
    return __awaiter(void 0, void 0, Promise, function () {
      var headers, response, responseData, data, error_1;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            deleteToken();
            headers = new Headers({
              Accept: "application/json",
              "Content-Type": "multipart/form-data",
            });
            _a.label = 1;
          case 1:
            _a.trys.push([1, 4, , 5]);
            return [
              4 /*yield*/,
              fetch(url, {
                method: "POST",
                headers: headers,
                body: formdata,
              }),
            ];
          case 2:
            response = _a.sent();
            return [4 /*yield*/, response.text()];
          case 3:
            responseData = _a.sent();
            data = JSON.parse(responseData);
            console.warn("formData: ", data);
            if (data[0].status === "success") {
              return [2 /*return*/, data[0].token];
            } else {
              return [2 /*return*/, -1];
            }
            return [3 /*break*/, 5];
          case 4:
            error_1 = _a.sent();
            return [2 /*return*/, -2];
          case 5:
            return [2 /*return*/];
        }
      });
    });
  },
  get: function (url) {
    return __awaiter(void 0, void 0, Promise, function () {
      var auth, headers, response, data, error_2;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            _a.trys.push([0, 4, , 5]);
            return [4 /*yield*/, getToken()];
          case 1:
            auth = _a.sent();
            if (!auth) {
              throw new Error("missing user data");
            }
            headers = new Headers({
              Authorization: token,
              Accept: "application/json",
            });
            return [
              4 /*yield*/,
              fetch(url, {
                method: "GET",
                headers: headers,
              }),
            ];
          case 2:
            response = _a.sent();
            return [4 /*yield*/, response.json()];
          case 3:
            data = _a.sent();
            return [2 /*return*/, data];
          case 4:
            error_2 = _a.sent();
            logError(url, error_2);
            return [2 /*return*/, error_2];
          case 5:
            return [2 /*return*/];
        }
      });
    });
  },
  getNoAuth: function (url) {
    return __awaiter(void 0, void 0, Promise, function () {
      var headers, response, data, error_3;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            _a.trys.push([0, 3, , 4]);
            headers = new Headers({
              Accept: "application/json",
            });
            return [
              4 /*yield*/,
              fetch(url, {
                method: "GET",
                headers: headers,
              }),
            ];
          case 1:
            response = _a.sent();
            return [4 /*yield*/, response.json()];
          case 2:
            data = _a.sent();
            return [2 /*return*/, data];
          case 3:
            error_3 = _a.sent();
            logError(url, error_3);
            return [2 /*return*/, null];
          case 4:
            return [2 /*return*/];
        }
      });
    });
  },
  //nel caso di errore non metti campi custom, procedi con il pagamento con stripe
  getNoAuthCart: function (url) {
    return __awaiter(void 0, void 0, Promise, function () {
      var headers, response, data, error_4;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            _a.trys.push([0, 3, , 4]);
            headers = new Headers({
              Accept: "application/json",
            });
            return [
              4 /*yield*/,
              fetch(
                "https://testarea.teyuto.tv/app/api/v1/business/?f=get_custom_field",
                {
                  method: "GET",
                  headers: headers,
                }
              ),
            ];
          case 1:
            response = _a.sent();
            return [4 /*yield*/, response.json()];
          case 2:
            data = _a.sent();
            return [2 /*return*/, data];
          case 3:
            error_4 = _a.sent();
            logError(url, error_4);
            return [2 /*return*/, null];
          case 4:
            return [2 /*return*/];
        }
      });
    });
  },
  post: function (url, formdata) {
    return __awaiter(void 0, void 0, Promise, function () {
      var auth, headers, response, data, error_5, error_6;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            _a.trys.push([0, 7, , 8]);
            return [4 /*yield*/, getToken()];
          case 1:
            auth = _a.sent();
            if (!auth) {
              throw new Error("missing user data");
            }
            headers = new Headers({
              Authorization: auth,
              Accept: "application/json",
              "Content-Type": "multipart/form-data",
            });
            _a.label = 2;
          case 2:
            _a.trys.push([2, 5, , 6]);
            return [
              4 /*yield*/,
              fetch(url, {
                method: "POST",
                headers: headers,
                body: formdata,
              }),
            ];
          case 3:
            response = _a.sent();
            return [4 /*yield*/, response.json()];
          case 4:
            data = _a.sent();
            return [2 /*return*/, data || true];
          case 5:
            error_5 = _a.sent();
            return [2 /*return*/, "sued"];
          case 6:
            return [3 /*break*/, 8];
          case 7:
            error_6 = _a.sent();
            logError(url, error_6);
            return [2 /*return*/, { url: url, error: error_6 }];
          case 8:
            return [2 /*return*/];
        }
      });
    });
  },
  postNoAuth: function (url, formdata) {
    return __awaiter(void 0, void 0, Promise, function () {
      var headers, response, data, error_7;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            _a.trys.push([0, 3, , 4]);
            headers = new Headers({
              Accept: "application/json",
              "Content-Type": "multipart/form-data",
            });
            return [
              4 /*yield*/,
              fetch(url, {
                method: "POST",
                headers: headers,
                body: formdata,
              }),
            ];
          case 1:
            response = _a.sent();
            return [4 /*yield*/, response.json()];
          case 2:
            data = _a.sent();
            return [2 /*return*/, data || true];
          case 3:
            error_7 = _a.sent();
            logError(url, error_7);
            return [2 /*return*/, null];
          case 4:
            return [2 /*return*/];
        }
      });
    });
  },
  uploadImage: function (url, title, description, collection_id, image) {
    try {
      var xhr = new XMLHttpRequest();
      xhr.open("POST", url);
      var formdata = new FormData();
      formdata.append("title", title);
      formdata.append("description", description);
      formdata.append("collection", collection_id);
      formdata.append("file", {
        name: image.fileName,
        type: image.type,
        uri:
          react_native_1.Platform.OS === "android"
            ? image.uri
            : image.uri.replace("file://", ""),
      });
      xhr.setRequestHeader("Authorization", token);

      xhr.send(formdata);
      return true;
    } catch (error) {
      logError(url, error);
      return null;
    }
  },
};
var logError = function (url, error) {};
exports["default"] = http;
