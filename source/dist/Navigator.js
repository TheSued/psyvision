"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_native_safe_area_context_1 = require("react-native-safe-area-context");
var native_1 = require("@react-navigation/native");
var stack_1 = require("@react-navigation/stack");
var Screen = require("./views");
var providers_1 = require("./providers");
var views_1 = require("./views");
var Stack = stack_1.createStackNavigator();
function Navigator() {
    return (react_1["default"].createElement(providers_1.ActiveCompanyContext.Consumer, null, function (context) {
        return context.isLoading ? (react_1["default"].createElement(views_1.Splashscreen, null)) : (react_1["default"].createElement(react_native_safe_area_context_1.SafeAreaProvider, null,
            react_1["default"].createElement(native_1.NavigationContainer, null,
                react_1["default"].createElement(Stack.Navigator, { headerMode: "none" }, context.activeCompany.home_guest ? (react_1["default"].createElement(react_1["default"].Fragment, null,
                    react_1["default"].createElement(Stack.Screen, { name: "Homepage", component: Screen.Homepage }),
                    react_1["default"].createElement(Stack.Screen, { name: "Searchpage", component: Screen.Searchpage }),
                    react_1["default"].createElement(Stack.Screen, { name: "Profile", component: Screen.Profile }),
                    react_1["default"].createElement(Stack.Screen, { name: "ManageLive", component: Screen.ManageLive }),
                    react_1["default"].createElement(Stack.Screen, { name: "AddLive", component: Screen.AddLive }),
                    react_1["default"].createElement(Stack.Screen, { name: "Player", component: Screen.Player }),
                    react_1["default"].createElement(Stack.Screen, { name: "StreamView", component: Screen.StreamView }),
                    react_1["default"].createElement(Stack.Screen, { name: "DebuggerView", component: Screen.DebuggerView }),
                    react_1["default"].createElement(Stack.Screen, { name: "SerieView", component: Screen.SerieView }),
                    react_1["default"].createElement(Stack.Screen, { name: "Login", component: Screen.LoginForm }),
                    react_1["default"].createElement(Stack.Screen, { name: "Registration", component: Screen.RegistrationForm }),
                    react_1["default"].createElement(Stack.Screen, { name: "WebViewPage", component: Screen.WebViewPage }))) : !context.user ? (react_1["default"].createElement(react_1["default"].Fragment, null,
                    react_1["default"].createElement(Stack.Screen, { name: "Login", component: Screen.LoginForm }),
                    react_1["default"].createElement(Stack.Screen, { name: "Registration", component: Screen.RegistrationForm }),
                    react_1["default"].createElement(Stack.Screen, { name: "WebViewPage", component: Screen.WebViewPage }))) : (react_1["default"].createElement(react_1["default"].Fragment, null,
                    react_1["default"].createElement(Stack.Screen, { name: "Homepage", component: Screen.Homepage }),
                    react_1["default"].createElement(Stack.Screen, { name: "Searchpage", component: Screen.Searchpage }),
                    react_1["default"].createElement(Stack.Screen, { name: "Profile", component: Screen.Profile }),
                    react_1["default"].createElement(Stack.Screen, { name: "ManageLive", component: Screen.ManageLive }),
                    react_1["default"].createElement(Stack.Screen, { name: "AddLive", component: Screen.AddLive }),
                    react_1["default"].createElement(Stack.Screen, { name: "Player", component: Screen.Player }),
                    react_1["default"].createElement(Stack.Screen, { name: "StreamView", component: Screen.StreamView }),
                    react_1["default"].createElement(Stack.Screen, { name: "DebuggerView", component: Screen.DebuggerView }),
                    react_1["default"].createElement(Stack.Screen, { name: "SerieView", component: Screen.SerieView }),
                    react_1["default"].createElement(Stack.Screen, { name: "WebViewPage", component: Screen.WebViewPage }),
                    react_1["default"].createElement(Stack.Screen, { name: "CastPage", component: Screen.Client })))))));
    }));
}
exports["default"] = Navigator;
