export function formatVideoObject(videoInfo: any) {
  if (videoInfo) {
    let video: any = {};
    // videoinfotype
    console.log("Type videoInfo.type --> ", videoInfo.type);

    if (videoInfo.type == "ondemand") {
      console.log("sono stato triggerato e stampo video ", videoInfo);
      video = {
        live: 0,
        started: 1,
      };

      console.log("VIDEO____1 -----> ", video);
      if (videoInfo.source_fullhd) {
        video = {
          ...video,
          fullhd: videoInfo.source_fullhd,
        };
      }
      if (videoInfo.source_hd) {
        video = {
          ...video,
          hd: videoInfo.source_hd,
        };
      }
      if (videoInfo.source_hq) {
        video = {
          ...video,
          hq: videoInfo.source_hq,
        };
      }
      if (videoInfo.source_original) {
        video = {
          ...video,
          original: videoInfo.source_original,
        };
      }
    } else {
      video = {
        live: 1,
        started: 0,
      };
      if (videoInfo.source_hls) {
        video = {
          ...video,
          hls: videoInfo.source_hls,
        };
      } else {
        video = {
          ...video,
          hls: "",
        };
      }
    }

    console.log("VIDEO____2 -----> ", video);
    return video;
  } else {
    console.log(
      "App tried to format a video object with incorrect data: ",
      videoInfo
    );
    return 0;
  }
}

export function formatCustomCartObject(cartInfo: any) {
  if (cartInfo) {
    let cartData: any = {};
    cartInfo.forEach((data) => {
      if (data.val.id == "country") {
        if (data) {
          cartData = {
            ...cartData,
            countryCheckPrice: data.val.checkprice,
            countryRequired: data.required,
          };
        }
      }

      if (data.val.id == "fullname") {
        if (data) {
          cartData = {
            ...cartData,
            fullnameCheckPrice: data.val.checkprice,
            fullnameRequired: data.required,
          };
        }
      }

      if (data.val.id == "fiscal_code") {
        if (data) {
          cartData = {
            ...cartData,
            taxCheckPrice: data.val.checkprice,
            taxRequired: data.required,
          };
        }
      }

      if (data.val.id == "checkbox-iscompany") {
        if (data) {
          cartData = {
            ...cartData,
            checkboxCheckPrice: data.val.checkprice,
            checkboxValue: data.val.value,
            checkboxChecked: data.val.checked,
            checkboxLabel: data.val.label,
            checkboxRequired: data.required,
            checkboxControlShowInput: data.control_show_input,
            checkboxControl: data.control,
          };
        }
      }

      if (data.val.id == "city") {
        if (data) {
          cartData = {
            ...cartData,
            cityCheckPrice: data.val.checkprice,
            cityRequired: data.required,
          };
        }
      }

      if (data.val.id == "province") {
        if (data) {
          cartData = {
            ...cartData,
            provinceCheckPrice: data.val.checkprice,
            provinceRequired: data.required,
          };
        }
      }

      if (data.val.id == "cap") {
        if (data) {
          cartData = {
            ...cartData,
            capCheckPrice: data.val.checkprice,
            capRequired: data.required,
          };
        }
      }

      if (data.val.id == "sdi_code_pec") {
        if (data) {
          cartData = {
            ...cartData,
            sdicodepecCheckPrice: data.val.checkprice,
            sdicodepecRequired: data.required,
          };
        }
      }

      if (data.val.id == "vat_num") {
        if (data) {
          cartData = {
            ...cartData,
            vatnumCheckPrice: data.val.checkprice,
            vatnumPlaceholder: data.val.placeholder,
            vatnumValidation: data.validation_vat_num,
            vatnumRequired: data.required,
          };
        }
      }

      if (data.val.id == "phone") {
        if (data) {
          cartData = {
            ...cartData,
            phoneCheckPrice: data.val.checkprice,
            phoneRequired: data.required,
          };
        }
      }

      if (data.val.id == "email_for_invoice") {
        if (data) {
          cartData = {
            ...cartData,
            emailInvoiceCheckPrice: data.val.checkprice,
            emailInvoiceRequired: data.required,
          };
        }
      }
    });

    return cartData;
  } else {
    console.log(
      "App tried to format a video object with incorrect data: ",
      cartInfo
    );
    return 0;
  }
}
