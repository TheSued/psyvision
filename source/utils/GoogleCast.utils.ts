import React from "react";
import { useRemoteMediaClient } from "react-native-google-cast";

const castState = () => {
  const client = useRemoteMediaClient();

  if (client) {
    return true;
  } else {
    return false;
  }
};
export default castState;
