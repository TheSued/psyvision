import { StyleSheet, Platform, StatusBar } from "react-native";

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  paddingBottom: {
    paddingBottom: Platform.OS === "android" ? 45 : 8,
  },
  paddingTop: {
    paddingTop:
      Platform.OS == "android" ? (StatusBar.currentHeight || 0) + 3 : 20,
  },
});

export default styles;
