import React from 'react';
import {SafeAreaView, ScrollView} from 'react-native';
import styles from './styles';
import AppStatusBar from '../AppStatusBar/AppStatusBar';

const ScrollViewUI = (props: any) => {
  const {style, children, header, backgroundColor} = props;
  const {flex, paddingTop, paddingBottom} = styles;
  return (
    <>
      <AppStatusBar fullscreen={props.fullscreen} />
      <SafeAreaView
        style={[
          flex,
          paddingTop,
          {
            backgroundColor: backgroundColor,
          },
          style,
        ]}>
        {header || null}

        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          style={flex}
          contentContainerStyle={[{flexGrow: 1}, style, paddingBottom]}>
          {children}
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default ScrollViewUI;
