import React from "react";
import { Text } from "react-native";
import { ActiveCompanyContext } from "../../providers";
import Colors from "../../environments/Colors";

class AppText extends React.PureComponent {
  static contextType = ActiveCompanyContext;

  state = {
    clearedText: "",
  };

  clearText() {
    let text = this.props.children;
    if (typeof text == "string") {
      firstPass = text.replace("&quot;", "'");
      secondPass = firstPass.replace("&#039;", '"');
      this.setState({ clearedText: secondPass });
    } else {
      this.setState({ clearedText: text });
    }
  }

  componentDidMount() {
    this.clearText();
  }

  componentDidUpdate() {
    this.clearText();
  }

  render() {
    const { style, children, time, ...rest } = this.props;
    const { clearedText } = this.state;

    return (
      <Text
        key={children}
        style={[{ color: this.context.fontColor }, style]}
        {...rest}
      >
        {time ? children : clearedText}
      </Text>
    );
  }
}

export { AppText };
