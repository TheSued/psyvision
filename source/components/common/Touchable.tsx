import React from 'react';
import {
  Platform,
  View,
  TouchableWithoutFeedback,
  StyleSheet,
} from 'react-native';
import Colors from '../../environments/Colors';

class Touchable extends React.Component {
  state = {
    hasFocus: false,
  };

  render() {
    if (Platform.isTV) {
      return (
        <TouchableWithoutFeedback
          onPress={() => this.props.onPress()}
          onFocus={() => this.setState({hasFocus: true})}
          onBlur={() => this.setState({hasFocus: false})}>
          <View
            style={[
              this.props.style,
              this.state.hasFocus ? styles.focusBorder : {},
            ]}>
            {this.props.children}
          </View>
        </TouchableWithoutFeedback>
      );
    } else {
      return (
        <TouchableWithoutFeedback onPress={() => this.props.onPress()}>
          <View style={this.props.style}>{this.props.children}</View>
        </TouchableWithoutFeedback>
      );
    }
  }
}

export {Touchable};

const border = Platform.isTV
  ? {borderColor: Colors.BorderTv, borderWidth: 2}
  : {};

const styles = StyleSheet.create({focusBorder: border});
