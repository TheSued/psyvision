import React, { Component } from 'react';
import {
	View,
	Animated,
} from 'react-native';

class Fade extends Component {

	componentWillMount() {
		this.alphaValue = new Animated.Value(0)
	}

	componentDidUpdate() {
		Animated.timing(
		  this.alphaValue,
		  {
		    toValue: this.props.controller ? 1 : 0,
		    duration: 500,
		  }
		).start()
	}


	render() {
		return(
				<Animated.View
					style= {{ 
						color: this.alphaValue.interpolate({
										  inputRange: [0, 1],
										  outputRange: [this.props.params.start, this.props.params.end]
										}),
						...this.props.style }}>
					{this.props.children}
				</Animated.View>
			)
	}
}


export { Fade };