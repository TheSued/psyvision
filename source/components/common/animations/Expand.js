import React, { Component } from 'react'
import {
  View,
  Animated,
} from 'react-native'

class Expand extends Component {
  constructor() {
    super();
    this.heightValue = new Animated.Value(0);
  }

  componentDidUpdate() {
    const { start, end } = this.props.params

    Animated.timing(
      this.heightValue,
      {
        toValue: this.props.controller ? end : start,
        duration: 200,
        useNativeDriver: false,
      }
    ).start()
  }

  render() {
    const { style, children, params, ...rest } = this.props
    return (
      <Animated.View style={[{ height: this.heightValue }, style]} {...rest} >
        {children}
      </Animated.View>
    )
  }
}


export { Expand }

// { height: this.heightValue }
// {transform: [{ scaleX: this.heightValue }]}