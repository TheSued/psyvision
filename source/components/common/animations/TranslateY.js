import React, { Component } from 'react'
import {
	Animated
} from 'react-native'


class TranslateY extends Component {
	
	componentWillMount() {
		this.animatedPosition = new Animated.Value(0)
	}

	componentDidUpdate() {
		const { start, end } = this.props.params

		Animated.timing(
		  this.animatedPosition,
		  {
		    toValue: this.props.controller ? end : start,
		    duration: 200,
		    // useNativeDriver: true,
		  }
		).start()
	}

	render() {
		const { style, children, params, ...rest } = this.props
		return(
				<Animated.View style={[{transform: [{ translateY: this.animatedPosition }]}, style ]} {...rest} >
					{children}
				</Animated.View>
			)
	}
}

export { TranslateY }