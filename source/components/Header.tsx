import React from "react";
import {
  View,
  Image,
  StyleSheet,
  Platform,
  Button,
  SafeAreaView,
  FlatList,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";
import LinearGradient from "react-native-linear-gradient";

import { ActiveCompanyContext } from "../providers/CompanyProvider";
import Colors from "../environments/Colors";
import { Input, Expand, AppText, Touchable } from "./common";
import { Modal } from "./";

import Routes from "../environments/Routes";
import * as Network from "../network";

class Header extends React.PureComponent {
  static contextType = ActiveCompanyContext;

  constructor(props: any) {
    super(props);
    this.state = {
      // searchState: false,
      // categoryState: false,
      // categories: [],
      // selectedCategories: [],
      // searchText: '',
    };
  }

  renderLogo() {
    const { activeCompany } = this.context;
    if (activeCompany.logo) {
      return { uri: activeCompany.logo };
    }
  }

  onPressCategory() {
    if (!this.state.categoryState) {
      this.getCategories();
    }
    this.setState({ categoryState: !this.state.categoryState });
  }

  setModalVisible() {
    this.setState({ modalVisible: !this.state.modalVisible });
  }

  categorySelect = (index: any) => {
    const categories = this.state.categories;
    categories[index].selected = !categories[index].selected;
    this.setState({
      categories,
      selectedCategories: categories.filter((el) => el.selected),
    });
  };

  getSelected() {
    return this.state.categories.filter((el) => el.selected);
  }

  renderInput() {
    if (this.state.searchState) {
      return (
        <View style={{ flexDirection: "row", flex: 5 }}>
          <View style={styles.inputStyle}>
            <Input
              style={{
                backgroundColor: "rgba(255,255,255,.2)",
                padding: 6,
                borderRadius: 5,
                fontSize: 18,
                marginTop: 8,
              }}
              placeholder={global.strings.SearchPlaceholder}
              autoCapitalize={"none"}
              autoCorrect={false}
              value={this.state.search}
              onChangeText={(text: string) =>
                this.setState({ searchValue: text })
              }
            />
          </View>

          <Button title="Category" onPress={() => this.onPressCategory()} />
        </View>
      );
    } else {
      return null;
    }
  }

  renderBack() {
    if (this.props.home) {
      if (this.context.user) {
        return (
          <View style={{ height: 30, width: 30 }}>
            <Icon
              name="menu"
              size={30}
              color={this.context.activeCompany.font_color || Colors.Text1}
            />
          </View>
        );
      } else if (!this.context.user) {
        return (
          <Image
            source={{ uri: Routes.NoProfileImg }}
            style={styles.profileImage}
          />
        );
      } else {
        return <AppText>{global.strings.Login}</AppText>;
      }
    } else {
      return (
        <View style={{ height: 30, width: 30 }}>
          <Icon
            name="arrow-back"
            size={30}
            color={this.context.activeCompany.font_color || Colors.Text1}
          />
        </View>
      );
    }
  }

  renderView() {
    return (
      <View style={styles.companyContainer}>
        <View style={{ flex: 1 }}>
          <Touchable onPress={this.props.onMenuPress}>
            {this.renderBack()}
          </Touchable>
        </View>
        <View style={{ flex: 2 }}>
          <Touchable
            style={{ flex: 1, alignItems: "center" }}
            onPress={this.props.onLogoPress}
          >
            <Image
              resizeMode={"contain"}
              source={{ uri: this.context.activeCompany.logo }}
              style={{ height: 30, width: 125 }}
            />
          </Touchable>
        </View>
        <View
          style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end" }}
        >
          {this.props.castButton && (
            <View
              style={{
                height: 30,
                width: 30,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {this.props.castButton}
            </View>
          )}
          {this.props.onCameraPress &&
            !Platform.isTV &&
            this.context.user &&
            this.context.user.creator == 1 &&
            this.context.activeCompany.button_creator && (
              <Touchable
                style={{ height: 30, width: 30 }}
                onPress={this.props.onCameraPress}
              >
                <Icon
                  name={"videocam"}
                  size={30}
                  color={this.context.activeCompany.font_color || Colors.Text1}
                />
              </Touchable>
            )}
          {this.props.onAddPress &&
            !Platform.isTV &&
            this.context.user &&
            this.context.user.creator == 1 &&
            this.context.activeCompany.button_creator && (
              <Touchable onPress={this.props.onAddPress}>
                <LinearGradient
                  colors={[Colors.Secondary1, Colors.Secondary2]}
                  start={{ x: 0.0, y: 0.5 }}
                  end={{ x: 1.0, y: 0.5 }}
                  style={{
                    height: 30,
                    width: 30,
                    borderRadius: 15,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <View
                    style={{
                      backgroundColor:
                        this.props.color !== undefined &&
                        this.props.color !== ""
                          ? this.props.color
                          : Colors.Background2,
                      justifyContent: "center",
                      alignItems: "center",
                      borderRadius: 14,
                      height: 28,
                      width: 28,
                    }}
                  >
                    <Icon
                      name={"add"}
                      size={20}
                      color={
                        this.context.activeCompany.font_color || Colors.Text1
                      }
                    />
                  </View>
                </LinearGradient>
              </Touchable>
            )}

          {this.props.onSearchPress &&
            this.context.activeCompany.button_search &&
            this.context.user && (
              <Touchable
                style={{ height: 30, width: 38 }}
                onPress={this.props.onSearchPress}
              >
                <Icon
                  name="search"
                  size={30}
                  color={this.context.activeCompany.font_color || Colors.Text1}
                  style={{ marginLeft: 8 }}
                />
              </Touchable>
            )}
        </View>
      </View>
    );
  }

  render() {
    const { categories, categoryState, modalVisible } = this.state;
    return this.renderView();
  }
}

const styles = StyleSheet.create({
  expandStyle: {
    marginLeft: 5,
    marginRight: 5,
  },
  companyContainer: {
    marginHorizontal: 8,
    marginBottom: 8,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
  },
  profileImage: {
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: "rgb(48,48,48)",
  },
  textStyle: {
    fontSize: 17,
    fontWeight: "700",
    borderBottomColor: "white",
    borderBottomWidth: 2,
    color: "white",
  },
  categoryItemStyle: {
    margin: 5,
  },
  buttonText: {
    opacity: 0.5,
    backgroundColor: "white",
  },
  inputStyle: {
    width: "70%",
  },
});

export { Header };
