import * as Keychain from 'react-native-keychain';

const Storage = {
  getToken: async (): Promise<string> => {
    try {
      const storageValue = await Keychain.getGenericPassword();
      if (storageValue) {
        return storageValue.password;
      }
      throw new Error('No user token found in storage');
    } catch (e) {
      return '';
    }
  },
  saveToken: async (token: string): Promise<void> => {
    try {
      await Keychain.setGenericPassword('loginToken', token);
    } catch (error) {
      // token already in storage
      return;
    }
  },
  deleteToken: async (): Promise<void> => {
    const deleteRes = await Keychain.resetGenericPassword();
    console.log('deletee',deleteRes)
  },
};

export default Storage;
