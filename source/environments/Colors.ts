export default Object.freeze({
  Background1: "rgba(255, 255, 255, 0.07)", // TextInput background
  Background2: "#14171c", // Default View Background
  Background3: "white",

  Secondary1: "rgba(255, 255, 255, 0.1)", // Button gradient1
  Secondary2: "rgba(255, 255, 255, .1)", // Button gradient2

  Inactive: "#999999",

  Text1: "#fff", // White text
  Text2: "#99989b", // Job at company text
  Text3: "black", // black text
  Text4: "red",
  TextError: "#f00",
  PlaceholderText: "rgba(255, 255, 255, 0.3)",
  BorderTv: "rgba(255, 255, 255, 0.5)",
  White08: "rgba(255, 255, 255, 0.8)",
  InputUnderline: "black",
  Shadow1: "#24b2e1",

  ModalColor: "rgba(0, 0, 0, 1)",
  ModalBackground: "rgba(0, 0, 0, 0.6)",
  Border1: "transparent",

  Indicator: "rgba(255, 255, 255, 0.7)",
});
