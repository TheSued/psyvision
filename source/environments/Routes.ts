import { AppCompanyId } from "./AppEnvironment";
import { Platform } from "react-native";
const BaseRoute = "https://teyuto.tv/app";

//const BaseRoute =
//'https://20211103t163136-dot-teyuto-live.ey.r.appspot.com/';
const ApiRoute = `${BaseRoute}/api/v1`;

// const CartApiRoute = `${CartBaseRoute}/api/v1/business`;
const appSuffix = "&app=1";
const companyIdSuffix = `&cid=${AppCompanyId}`;
const CartBaseRoute =
  "https://testarea.teyuto.tv/app/api/v1/business/?f=get_custom_field";
const videoApiRoute = "https://testarea.teyuto.tv/app/api/v1";

export default Object.freeze({
  // Assets
  NoProfileImg: `${BaseRoute}/img/no_img_profile_u.png`,

  // User
  Login: `${ApiRoute}/user/?f=login${appSuffix}${companyIdSuffix}`,
  SetDeviceToken: `${ApiRoute}/user/?f=set_device_token${appSuffix}`,
  ProfileDataRoute: `${ApiRoute}/user/?f=user_data${appSuffix}`,
  changeCompany: `${ApiRoute}/user/?f=change_company${appSuffix}`, // POST: idc
  GetPaymentInfo: `${ApiRoute}/user/?f=get_last_info_pay${appSuffix}`,
  Register: `${ApiRoute}/user/?f=register${appSuffix}${companyIdSuffix}`,
  ChangeLanguage: `${ApiRoute}/user/?f=update_user_language`,

  //Company
  CompanySettings: `${ApiRoute}/user/?f=settings_company${appSuffix}`,
  HomepageSettings: `${ApiRoute}/landing/?f=get_homepage_settings${appSuffix}`,

  // Serie
  GetCollections: `${ApiRoute}/collection/?f=get_collection_user${appSuffix}`,
  GetSeries: `${ApiRoute}/collection/?f=get_collection${appSuffix}&platform=${Platform.OS}`, // step & company & (search) & (parent = 1 (bundle)) & collection = idcollection (per prendere le info)
  GetBundleChild: `${ApiRoute}/collection/?f=get_child_series${appSuffix}`, // id & step
  GetSerieParent: `${ApiRoute}/collection/?f=get_parent_series${appSuffix}`, // id

  // Video
  GetVideo: `${ApiRoute}/video/?f=get_video${appSuffix}`, // step & company & (search) & (video = idVideo) & ('scheduled' || 'ondemand' || 'reminder' || 'live') & (collection = serieId)
  GetVideoKeepWatching: `${ApiRoute}/video/?f=get_keep_watching${appSuffix}`, // step & company & (search) & (video = idVideo) & ('scheduled' || 'ondemand' || 'reminder' || 'live') & (collection = serieId)
  AddLive: `${ApiRoute}/video/?f=add_live${appSuffix}`, // per add video file1 in POST
  GetAttachments: `${ApiRoute}/video/?f=get_file${appSuffix}`, // &video=$id_video
  VideoSource: `${ApiRoute}/video/?f=get_video_source${appSuffix}`, // POST con id video returna video_url_source
  PlayVideo: `${ApiRoute}/video/?f=action_enter${appSuffix}`, // clicco play // POST id - idvideo, time (in sec)
  UpdateVideo: `${ApiRoute}/video/?f=action_update${appSuffix}`, //
  ViewersCounter: `${ApiRoute}/video/?f=info_view${appSuffix}`, // get viewers // id=id_video
  GetVideoTest: `${videoApiRoute}/video/?f=test_get_video`,
  GetVideoTestPreview: `${ApiRoute}/messenger/?f=get_preview_url${appSuffix}`,
  GetVideoRelatedPreview: `${ApiRoute}/messenger/?f=get_preview_url${appSuffix}`,
  GetPreviewUrl: `${ApiRoute}/messenger/?f=get_preview_url${appSuffix}`,
  AddLike: `${ApiRoute}/video/?f=set_likes${appSuffix}`,
  GetSubtitles: `${ApiRoute}/video/?f=get_subtitles&video=`,
  // Category
  GetCategories: `${ApiRoute}/categories/?f=get_categories&show${appSuffix}`,
  // StreamForm Page Routes
  AddLiveWithCover: `${ApiRoute}/google/?f=secureUrl&url=/app/api/v1/video/?f=add_live${appSuffix}`,

  // Chat
  ListChat: `${ApiRoute}/video_chat/?f=list_chat${appSuffix}`, // POST con idl
  ListPartecipants: `${ApiRoute}/video_chat/?f=list_live_partecipants${appSuffix}`, // POST con idl
  ListQeA: `${ApiRoute}/video_chat/?f=list_qea_ondemand${appSuffix}`, // POST con idl
  SendMessage: `${ApiRoute}/video_chat/?f=send_message${appSuffix}`, // POST con idl, message, time_live, user, avatar
  SendQeA: `${ApiRoute}/video_chat/?f=send_qea_ondemand${appSuffix}`, // POST con idl & question
  AnswerQuestion: `${ApiRoute}/video_chat/?f=set_qea${appSuffix}`, // POST con id == idChat

  // Payments
  // Pay: `https://20200502t210227-dot-teyuto-live.ey.r.appspot.com/app/api/v1.1/pay/?f=create_payment${appSuffix}`, // create a paymentIntent
  Pay: `https://teyuto.tv/app/api/v1.1/pay/?f=create_payment${appSuffix}`, // create a paymentIntent
  ApplyCoupon: `${ApiRoute}/pay/?f=apply_coupon_series${appSuffix}`,
  DetailPricing: `https://testarea.teyuto.tv/app/api/v1.1/pay/?f=details_pricing_preview`,
  // Quiz
  Quiz: `${BaseRoute}/live/quiz`,

  CustCart: `${CartBaseRoute}​`,
});
