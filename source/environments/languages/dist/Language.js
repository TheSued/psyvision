"use strict";
exports.__esModule = true;
var react_native_1 = require("react-native");
var Strings_en_GB_1 = require("./Strings-en_GB");
var Strings_it_IT_1 = require("./Strings-it_IT");
var Strings_es_ES_1 = require("./Strings-es_ES");
var Strings_fr_FR_1 = require("./Strings-fr_FR");
function defineLanguage() {
    var locale = getLocale();
    if (locale.includes("it_")) {
        return Strings_it_IT_1["default"];
    }
    else if (locale.includes("en_")) {
        return Strings_en_GB_1["default"]; // default - english
    }
    else if (locale.includes("es_")) {
        return Strings_es_ES_1["default"]; // default - english
    }
    else if (locale.includes("fr_")) {
        return Strings_fr_FR_1["default"]; // default - english
    }
}
exports["default"] = defineLanguage;
function getLocale() {
    switch (react_native_1.Platform.OS) {
        case "ios":
            return ((react_native_1.NativeModules.SettingsManager.settings.AppleLocale &&
                react_native_1.NativeModules.SettingsManager.settings.AppleLocale.replace("-", "_")) ||
                react_native_1.NativeModules.SettingsManager.settings.AppleLanguages[0]);
        case "android":
            return react_native_1.NativeModules.I18nManager.localeIdentifier;
    }
    return "";
}
