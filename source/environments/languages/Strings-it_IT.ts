export default Object.freeze({
  // Signs
  Euro: "\u20AC",

  // General Strings
  RegistrationError: "Verifica che i dati inseriti siano corretti.",
  passwordLengthError: "La password deve essere lunga almeno 8 caratteri.",
  emailAlreadyRegisterd: "Email già registrata",
  GeneralError:
    "Si è verificato un errore, visita il sito web per maggiori informazioni",
  InvalidFields: "Campi non validi", // used in payment
  GeneralErrorShort: "Si è verificato un errore",
  Ok: "Ok",
  Back: "Indietro",
  Yes: "Si",
  No: "No",
  Cancel: "Annulla",
  For: "per",
  Close: "Chiudi",
  TryAgain: "Riprova",
  showAll: "Sfoglia tutti",

  // Login Strings
  SigningIn: "Accesso in corso...",
  SignIn: "Accedi",
  SignUp: "Registrati",
  Skip: "Salta",
  NoCredentials: "Inserisci email/password",
  CredentialsError: "Email e/o password non corretta",
  NetworkError: "Errore di rete",
  EmailPlaceholder: "Email",
  PasswordPlaceholder: "Password",

  // Registration Strings
  UsernamePlaceholder: "Username",
  RepeatPasswordPlaceholder: "Ripeti Password Email",
  NoAccount: "Non hai ancora un account?",
  MissingFormFields: "Riempire tutti i campi per registrarsi",
  MismatchingPasswords: "Le password non corrispondono",
  TermsAgreement: "Registrandoti accetti i seguenti ",
  PrivacyAgreement: "Registrandoti accetti le seguenti ",
  Terms: "termini di utilizzo *",
  PrivacyPolicy: "privacy policy",
  PromotionAgreement: "Vorrei ricevere promozioni via email",
  RegistrationSuccessfull: "Registrazione avvenuta con successo",
  TermsUnchecked: "Devi accettare i termini per registrarti",
  VerifyEmailError: "Email non verificata",
  VerifyEmail:
    "Controlla la posta in arrivo (anche SPAM) conferma la tua registrazione e riprova",

  // Profile Strings
  LogoutQuestion: "Sicuro di voler eseguire il logout?",
  NoLive: "Nessun live in programma trovato.",
  At: "presso ", // 'job' at 'company'
  Logout: "Logout",
  Privacy: "Privacy Policy",
  Apply: "Candidati",
  PrivacyLink: "https://weshort.com/app/channelPage?page=437",
  TermsOfService: "Termini di Servizio",
  TermsLink: "https://weshort.com/app/channelPage?page=434",

  // AddLive Strings
  LoadingCollections: "Carico serie...",
  NoCollection: "Nessuna serie trovata.",
  DescriptionPlaceholder: "Descrizione",
  TitlePlaceholder: "Titolo",
  Collection: "Serie",
  MissingFieldLive: "Riempi ogni campo per creare un nuovo live",
  MissingFieldVideo: "Riempi ogni campo per caricare un nuovo video",
  LiveCreated: "Diretta creata con successo",
  GoLive: "Vai in diretta",
  Later: "Più tardi",
  NoPermissions: "L'app non ha i permessi per accedere alla libreria",
  LoadPreview: "Carica una preview",

  // Catalog Strings
  Bundles: "Pacchetti",
  Series: "Serie",
  OnDemand: "On Demand",
  Live: "In diretta",
  Scheduled: "Programmati",
  KeepWatching: "Continua a guardare",
  SearchPlaceholder: "Cerca...",
  SearchCategory: "Categorie",
  ConfirmCategory: "Conferma",
  QuizAlert: "I quiz sono disponibili solo su teyuto.tv",
  NoVideoAccess: "Non puoi accedere a questo video",
  Bundle: "Pacchetto",

  // Serie Strings
  Subscribe: "Abbonati",
  Pay: "Paga",
  Or: "oppure",
  Month: "mese/i",
  Year: "anno",
  SerieIncluded: "All'interno di:",
  BundleContaining: "Questa sezione contiene:",
  Access1: "Abbonati sul sito ",
  Access2: " per guardare questo contenuto.",
  AccessContent: "Accedi al contenuto",
  ChoosePlan: "Scegli l'offerta e accedi ai contenuti",
  PaymentSuccess: "Il pagamento è andato a buon fine",
  PaymentFailed: "C'è stato un errore durante il pagamento",
  PaymentRedirect: "Verrai reindirizzato alla homepage tra 5 sec...",

  // StreamView Strings
  LIVE: "LIVE",
  AnswerQuestion1: "Sicuro di voler rispondere alla domanda di\n ",
  AnswerQuestion2: "?",
  StreamPermissions: "L'app ha bisogno dei permessi\n per andare in diretta",
  PermissionsDenied: "Impossibile andare live senza permessi",
  Answer: "Rispondi",
  Ignore: "Ignora",
  AnswerSuccessfull: "Stai rispondendo alla domanda",

  // Player Strings
  AnswerAt1: "- risposta a ",
  AnswerAt2: "secondi",
  QuestionSent: "Commento inviato",
  LiveNotStarted: "La diretta non è ancora iniziata",
  Attachments: "Allegati",
  DownloadSuccessfull: "File scaricato",
  StoragePermissionsError: "L'app ha bisogno dei permessi per salvare il file",
  VideoError: "C'è stato un errore nel caricamento del video",
  CastQuestion: "Smart TV disponibile, vuoi trasmettere il video alla TV?",
  Cast: "Trasmetti",

  showTrailer: "Visualizza Trailer",

  // Chat Strings
  Q: "Q", // Question
  A: "A", // Answer
  LeaveComment: "Lascia un commento...",
  SaySomething: "Scrivi qualcosa...",

  // Payment Strings
  Pack: "Serie comprese nel pack: ",
  FullName: "Full name *",
  CompanyName: "Company name *",
  InvoiceQuestion: "Hai bisogno della fattura?",
  Address: "Indirizzo *",
  CAP: "CAP *",
  City: "Città *",
  Province: "Provincia *",
  Country: "Paese *",
  VAT: "Partita IVA *",
  FiscalCode: "Codice Fiscale *",
  InvoiceCode: "Codice destinazione",
  MissingFieldPayment: "Compila i campi obbligatori (*)",
  ApplyCoupon: "Usa coupon",
  SuccessfullCoupon: "Codice sconto applicato con successo",
  InvalidCoupon: "Codice sconto invalido",
  DiscountApplied: "Codice sconto applicato",
  Check: "Valida", // used in coupons modal
  DiscountAlreadyApplied: "Codice sconto già applicato",
  SuccessfullPayment: "Pagamento riuscito",
  InvalidPaymentMethod: "Metodo di pagamento non valido o non inserito",
  PaymentNotNeeded: "Iscritto con successo",
  DiscountCode: "Coupon",
  PaymentCanceled: "Pagamento annullato",
  PaymentDeclined: "Payment rifiutato",
  Next: "Avanti",
  PayWith: "Paga con ",
  NativeTokenError: "Si è verificato un errore con il pagamento nativo",
  useGiftcard:
    "Il codice applicato è una giftcard, vuoi usarlo ora e ricevere la serie/pacchetto gratis?",
});
