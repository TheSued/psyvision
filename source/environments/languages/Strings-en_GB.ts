export default Object.freeze({
  // Signs
  Euro: "\u20AC",

  // General Strings
  RegistrationError: "Check that the data entered is correct.",
  passwordLengthError: "The password must be at least 8 characters long.",
  emailAlreadyRegisterd: "Email already registered",
  GeneralError: "An error has occurred, please check the website for more info",
  InvalidFields: "Invalid Fields, please check again", // used in payment
  GeneralErrorShort: "An error has occurred",
  Ok: "Ok",
  Back: "Back",
  Yes: "Yes",
  No: "No",
  Cancel: "Cancel",
  For: "for",
  Close: "Close",
  TryAgain: "Try again",
  showAll: "Show more",

  // Login Strings
  SigningIn: "Signing in...",
  SignIn: "Sign in",
  SignUp: "Sign up",
  Skip: "Skip",
  NoCredentials: "Insert email/password",
  CredentialsError: "Incorrect email or password",
  NetworkError: "Network error",
  EmailPlaceholder: "Email",
  PasswordPlaceholder: "Password",

  // Registration Strings
  UsernamePlaceholder: "Username",
  RepeatPasswordPlaceholder: "Repeat password",
  NoAccount: "Don't have an account yet?",
  MissingFormFields: "Fill in all fields to register.",
  MismatchingPasswords: "Password fields mismatch",
  TermsAgreement: "By signing up you agree at the company's ",
  PrivacyAgreement: "By signing up you agree at the company's ",
  Terms: "terms",
  PrivacyPolicy: "privacy policy",
  RegistrationSuccessfull: "Registration successfull",
  PromotionAgreement: "Subscribe me for promotions",
  TermsUnchecked: "You need to agree to terms to register",
  VerifyEmailError: "Email not verified",
  VerifyEmail:
    "Check your inbox (including SPAM) confirm your registration and try again",

  // Profile Strings
  LogoutQuestion: "Are you sure you want to logout from the app?",
  NoLive: "You dont have any scheduled lives yet.",
  At: "at ", // 'job' at 'company'
  Logout: "Logout",
  Privacy: "Privacy Policy",
  Apply: "Apply",
  PrivacyLink: "https://weshort.com/app/channelPage?page=438",
  TermsOfService: "Terms of Service",
  TermsLink: "https://weshort.com/app/channelPage?page=435",

  // AddLive Strings
  LoadingCollections: "Loading series...",
  NoCollection: "No serie found.",
  DescriptionPlaceholder: "Description",
  TitlePlaceholder: "Title",
  Collection: "Serie",
  MissingFieldLive: "Fill every field to create a live",
  MissingFieldVideo: "Fill every field to upload a video",
  LiveCreated: "Live created successfully",
  GoLive: "Go live now",
  Later: "Later",
  NoPermissions: "The app require library permissions",
  LoadPreview: "Load an optional preview",

  // Catalog Strings
  Bundles: "Bundles",
  Series: "Series",
  OnDemand: "On Demand",
  Live: "Live",
  Scheduled: "Scheduled",
  KeepWatching: "Keep Watching",
  SearchPlaceholder: "Search...",
  SearchCategory: "Category",
  ConfirmCategory: "Confirm",
  QuizAlert: "Video quiz are available on teyuto.tv only",
  NoVideoAccess: "You can't access this video",
  Bundle: "Bundle",

  // Serie Strings
  Subscribe: "Subscribe",
  Pay: "Pay",
  Or: "or",
  Month: "month/s",
  Year: "year",
  SerieIncluded: "This serie is included in",
  BundleContaining: "This bundle contains",
  Access1: "Subscribe at ",
  Access2: " to watch this content.",
  AccessContent: "Access the content",
  ChoosePlan: "Choose the offer and access the contents",
  PaymentSuccess: "The payment was successful",
  PaymentFailed: "There was an error during the payment",
  PaymentRedirect: "You will be redirected to the homepage in 5 sec...",
  // StreamView Strings
  LIVE: "LIVE",
  AnswerQuestion1: "Are you sure you want to answer\n ",
  AnswerQuestion2: " question?",
  StreamPermissions:
    "The app require both camera and\n microphone permissions to go live",
  PermissionsDenied: "Unable to stream without permissions",
  Ignore: "Ignore",
  Answer: "Answer",
  AnswerSuccessfull: "Question answered",

  // Player Strings
  AnswerAt1: "- answer at ",
  AnswerAt2: "s",
  QuestionSent: "Comment sent",
  LiveNotStarted: "This live is not started yet",
  Attachments: "Attachments",
  DownloadSuccessfull: "File downloaded",
  StoragePermissionsError: "The app need permissions to save the file",
  VideoError: "There was an error retrieving the video",
  CastQuestion: "Cast avaible, do you want to cast the media?",
  Cast: "Cast",

  showTrailer: "View Trailer",

  // Chat Strings
  Q: "Q", // Question
  A: "A", // Answer
  LeaveComment: "Leave a comment...",
  SaySomething: "Say something...",

  // Payment Strings
  Pack: "Series included: ",
  FullName: "Full name *",
  CompanyName: "Company name *",
  InvoiceQuestion: "Do you need an invoice?",
  Address: "Address *",
  CAP: "CAP *",
  City: "City *",
  Province: "Province *",
  Country: "Country *",
  VAT: "VAT *",
  FiscalCode: "Fiscal code *",
  InvoiceCode: "Invoice code destination",
  MissingFieldPayment: "Missing required fields (*)",
  ApplyCoupon: "Apply coupon",
  SuccessfullCoupon: "Discount code succesfully applied",
  InvalidCoupon: "Invalid discount Code",
  DiscountApplied: "Discount applied",
  Check: "Check", // used in coupons modal
  DiscountAlreadyApplied: "Discount already applied",
  SuccessfullPayment: "Payment successfull",
  InvalidPaymentMethod: "Invalid/missing payment method",
  PaymentNotNeeded: "Successfully subscribed",
  DiscountCode: "Code",
  PaymentCanceled: "Payment canceled",
  PaymentDeclined: "Payment declined",
  Next: "Next",
  PayWith: "Pay with ",
  NativeTokenError: "There was an error with native pay",
  useGiftcard:
    "The code applied is a giftcard, do you want to use it now and get the serie/bundle for free?",
});
