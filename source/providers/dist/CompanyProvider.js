"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.ActiveCompanyConsumer = exports.ActiveCompanyProvider = exports.ActiveCompanyContext = void 0;
var react_1 = require("react");
exports.ActiveCompanyContext = react_1["default"].createContext({});
var ActiveCompanyProvider = /** @class */ (function (_super) {
    __extends(ActiveCompanyProvider, _super);
    function ActiveCompanyProvider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            font: "",
            backgroundColor: "",
            buttonColor: "",
            fontColor: "",
            alphaColor: "",
            languageCompanySettings: "",
            playerCurrentTime: 0,
            videoNotificationId: 0,
            isLoading: true,
            isSignedIn: false,
            user: null,
            paymentSuccessfull: false,
            activeCompany: {},
            companies: [],
            debugLog: [
                {
                    id: "0",
                    code: "exampleCode1",
                    message: "exampleMessage1"
                },
                {
                    id: "1",
                    code: "exampleCode2",
                    message: "exampleMessage2"
                },
                {
                    id: "2",
                    code: "exampleCode3",
                    message: "exampleMessage3"
                },
            ]
        };
        return _this;
    }
    ActiveCompanyProvider.prototype.render = function () {
        var _this = this;
        var children = this.props.children;
        return (react_1["default"].createElement(exports.ActiveCompanyContext.Provider, { value: __assign(__assign({}, this.state), { setVideoNotificationId: function (videoNotificationId) {
                    return _this.setState({ videoNotificationId: videoNotificationId });
                }, consumedVideoNotification: function () {
                    return _this.setState({ videoNotificationId: 0 });
                }, signedIn: function (user) { return _this.setState({ isLoading: false, user: user }); }, signedOut: function () {
                    return _this.setState({ isLoading: true, user: null }, function () {
                        return console.log("logout jndjhd");
                    });
                }, changeActiveCompany: function (company) {
                    _this.setState({ activeCompany: company });
                }, updateCompanyList: function (companyList, activeCompany) {
                    _this.setState({
                        companies: companyList,
                        activeCompany: activeCompany || companyList[0]
                    });
                }, updateContent: function (content) { return _this.setState({ content: content }); }, togglePaymentSuccessfull: function () {
                    return _this.setState({
                        paymentSuccessfull: !_this.state.paymentSuccessfull
                    });
                }, addDebugLog: function (log) {
                    return _this.setState({
                        debugLog: __spreadArrays(_this.state.debugLog, [
                            __assign({ id: "" + _this.state.debugLog.length }, log),
                        ])
                    });
                }, setPlayerCurrentTime: function (time) {
                    _this.setState({ playerCurrentTime: time });
                }, setCompanySettingsLanguage: function (language) {
                    _this.setState({ languageCompanySettings: language });
                }, handleCompanyColors: function (schema, primaryColor, backgroundColor, font) {
                    _this.setState({ font: font });
                    if (schema === "light") {
                        if (backgroundColor) {
                            _this.setState({ backgroundColor: backgroundColor });
                        }
                        else {
                            _this.setState({ backgroundColor: "white" });
                        }
                        _this.setState({ alphaColor: "rgba(0,0,0,0.24)" });
                        _this.setState({ fontColor: "black" });
                        if (primaryColor) {
                            _this.setState({ buttonColor: primaryColor });
                        }
                        else {
                            _this.setState({ buttonColor: "grey" });
                        }
                    }
                    else {
                        if (backgroundColor) {
                            _this.setState({ backgroundColor: backgroundColor });
                        }
                        else {
                            _this.setState({ backgroundColor: "black" });
                        }
                        _this.setState({ fontColor: "white" });
                        _this.setState({ alphaColor: "rgba(255, 255, 255, 0.07)" });
                        if (primaryColor) {
                            _this.setState({ buttonColor: primaryColor });
                        }
                        else {
                            _this.setState({ buttonColor: "grey" });
                        }
                    }
                } }) }, children));
    };
    return ActiveCompanyProvider;
}(react_1["default"].Component));
exports.ActiveCompanyProvider = ActiveCompanyProvider;
exports.ActiveCompanyConsumer = exports.ActiveCompanyContext.Consumer;
