import React from "react";

export const ActiveCompanyContext = React.createContext({});

export class ActiveCompanyProvider extends React.Component {
  state = {
    font: "",
    backgroundColor: "",
    buttonColor: "",
    buttonTextColor: "",
    fontColor: "",
    alphaColor: "",
    languageCompanySettings: "",
    actualPreviewUri: "",
    playerCurrentTime: 0,
    videoNotificationId: 0,
    isLoading: true,
    homepageFetchLoading: false,
    isSignedIn: false,
    user: null,
    paymentSuccessfull: false,
    activeCompany: {},
    companies: [],
    debugLog: [
      {
        id: "0",
        code: "exampleCode1",
        message: "exampleMessage1",
      },
      {
        id: "1",
        code: "exampleCode2",
        message: "exampleMessage2",
      },
      {
        id: "2",
        code: "exampleCode3",
        message: "exampleMessage3",
      },
    ],
  };

  render() {
    const { children } = this.props;
    return (
      <ActiveCompanyContext.Provider
        value={{
          ...this.state,
          setVideoNotificationId: (videoNotificationId: number) =>
            this.setState({ videoNotificationId }),
          consumedVideoNotification: () =>
            this.setState({ videoNotificationId: 0 }),
          signedIn: (user: any) => this.setState({ isLoading: false, user }),
          signedOut: () =>
            this.setState({ isLoading: true, user: null }, () =>
              console.log("logout jndjhd")
            ), // recalc user data to not write the code everywhere
          changeActiveCompany: (company: any) => {
            this.setState({ activeCompany: company });
          },
          updateCompanyList: (companyList: any[], activeCompany: any) => {
            this.setState({
              companies: companyList,
              activeCompany: activeCompany || companyList[0],
            });
          },
          updateContent: (content: any[]) => this.setState({ content }),
          togglePaymentSuccessfull: () =>
            this.setState({
              paymentSuccessfull: !this.state.paymentSuccessfull,
            }),
          addDebugLog: (log: any) =>
            this.setState({
              debugLog: [
                ...this.state.debugLog,
                {
                  id: "" + this.state.debugLog.length,
                  ...log,
                },
              ],
            }),
          setActualPreview: (uri: string) => {
            this.setState({ actualPreviewUri: uri });
          },
          setPlayerCurrentTime: (time: number) => {
            this.setState({ playerCurrentTime: time });
          },
          setCompanySettingsLanguage: (language: string) => {
            this.setState({ languageCompanySettings: language });
          },
          setHomePageFetchLoading: (value: boolean) => {
            this.setState({ homepageFetchLoading: value });
          },
          handleCompanyColors: (
            schema: string,
            primaryColor: string,
            backgroundColor: string,
            font: string
          ) => {
            this.setState({ font: font });
            this.setState({ buttonTextColor: "white" });

            if (schema === "light") {
              if (backgroundColor) {
                this.setState({ backgroundColor: backgroundColor });
              } else {
                this.setState({ backgroundColor: "white" });
              }
              this.setState({ alphaColor: "rgba(0,0,0,0.24)" });
              this.setState({ fontColor: "black" });
              if (primaryColor) {
                this.setState({ buttonColor: primaryColor });
              } else {
                this.setState({ buttonColor: "grey" });
              }
            } else {
              if (backgroundColor) {
                this.setState({ backgroundColor: backgroundColor });
              } else {
                this.setState({ backgroundColor: "black" });
              }
              this.setState({ fontColor: "white" });
              this.setState({ alphaColor: "rgba(255, 255, 255, 0.06)" });
              if (primaryColor) {
                this.setState({ buttonColor: primaryColor });
              } else {
                this.setState({ buttonColor: "grey" });
              }
            }
          },
        }}
      >
        {children}
      </ActiveCompanyContext.Provider>
    );
  }
}

export const ActiveCompanyConsumer = ActiveCompanyContext.Consumer;
