import React, { Component } from "react";
import IAP, {
  Product,
  Subscription,
  purchaseErrorListener,
  purchaseUpdatedListener,
  ProductPurchase,
  PurchaseError,
  SubscriptionPurchase,
} from "react-native-iap";
import {
  StyleSheet,
  Text,
  Platform,
  FlatList,
  View,
  Pressable,
  ActivityIndicator,
  EmitterSubscription,
  AppState,
} from "react-native";
import {
  OrientationLocker,
  PORTRAIT,
  LANDSCAPE,
} from "react-native-orientation-locker";
import { ActiveCompanyContext } from "../providers/CompanyProvider";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { getBundleId } from "react-native-device-info";
import * as Network from "../network";

// Platform select will allow you to use a different array of product ids based on the platform
// const items: any = Platform.select({
//   ios: [],
//   android: ["teyuto_10_1m", "supertette_20"],
// });
type IAPState = {
  parentSubscriptions: Subscription[];
  parentProducts: Product[];
  childSubscription: Subscription[];
  childProduct: Product[];
  appState: any;
  loading: boolean;
  paymentSuccessfull: boolean;
  paymentFailed: boolean;
};
type Parent = {
  description: string;
  id: string;
  product: string;
  subscription: string;
  title: string;
};

class InAppView extends Component {
  static contextType = ActiveCompanyContext;
  state: IAPState = {
    paymentSuccessfull: false,
    paymentFailed: false,
    parentSubscriptions: [],
    parentProducts: [],
    childSubscription: [],
    childProduct: [],
    appState: AppState.currentState,
    loading: true,
  };

  purchaseUpdateSubscription: EmitterSubscription | null = null;
  purchaseErrorSubscription: EmitterSubscription | null = null;
  appStateSubscription: any = null;

  validate_ANDROID: any = async (
    productId: any,
    purchaseToken: any,
    packageApp: string,
    id_series: any
  ) => {
    try {
      // send receipt to backend
      const response = await Network.validateIAP(
        productId,
        purchaseToken,
        packageApp,
        id_series
      );

      if (JSON.parse(JSON.stringify(response)).is_valid == "true") {
        this.setState({ loading: false, paymentSuccessfull: true }, () =>
          setTimeout(() => {
            this.props.navigation.reset({
              routes: [{ name: "Homepage" }],
            });
          }, 5000)
        );
      } else {
        this.setState({ loading: false, paymentFailed: true }, () =>
          setTimeout(() => {
            this.props.navigation.reset({
              routes: [{ name: "Homepage" }],
            });
          }, 5000)
        );
      }
    } catch (error: any) {}
  };
  openTerms() {
    this.props.navigation.navigate("WebViewPage", {
      source: this.context.activeCompany.terms,
    });
  }

  openPrivacyPolicy() {
    this.props.navigation.navigate("WebViewPage", {
      source: this.context.activeCompany.privacy_policy,
    });
  }
  validate_IOS: any = async (purchaseToken: any, id_series: any) => {
    try {
      // send receipt to backend
      const response = await Network.validateIosIAP(purchaseToken, id_series);

      if (JSON.parse(JSON.stringify(response)).is_valid == "true") {
        this.setState({ loading: false, paymentSuccessfull: true }, () =>
          setTimeout(() => {
            this.props.navigation.reset({
              routes: [{ name: "Homepage" }],
            });
          }, 5000)
        );
      } else {
        this.setState({ loading: false, paymentFailed: true }, () =>
          setTimeout(() => {
            this.props.navigation.reset({
              routes: [{ name: "Homepage" }],
            });
          }, 5000)
        );
      }
    } catch (error: any) {}
  };

  private parentId = new Map();

  renderItem: any = ({ item }: any) => {
    const { _parentPayments } = this.props.route.params;

    const parent: any = () => {
      if (_parentPayments.length > 0) {
        return _parentPayments.filter(
          (product: Parent) =>
            product.product === item.productId ||
            product.subscription === item.productId
        );
      } else {
        return [];
      }
    };

    return (
      <View
        style={[
          styles.productsContainer,
          {
            backgroundColor: this.context.backgroundColor + "40",
          },
        ]}
      >
        <View style={{ flex: 2, marginVertical: 4 }}>
          <Text
            style={{
              color: this.context.buttonColor,
              fontSize: 22,
              fontWeight: "bold",
            }}
          >
            {item.title}
          </Text>
        </View>
        <View style={{ flex: 2, marginVertical: 4 }}>
          <Text style={{ color: this.context.fontColor }}>
            {parent().length ? parent()[0].description : item.description}
          </Text>
        </View>

        <View style={{ flex: 2, marginVertical: 4, width: "100%" }}>
          <Pressable
            onPress={async () => {
              this.setState({ loading: true });
              if (parent().length) {
                this.parentId.set("id", parent()[0].id);
              }

              if (item.type === "inapp" || item.type === "iap") {
                try {
                  await IAP.requestPurchase(item.productId);
                } catch (err: any) {
                  console.warn(err.code, err.message);
                }
              } else if (item.type === "subs" || item.type === "sub") {
                try {
                  await IAP.requestSubscription(item.productId);
                } catch (err: any) {
                  console.warn(err.code, err.message);
                }
              }
            }}
            style={({ pressed }) => [
              {
                backgroundColor: pressed
                  ? this.context.buttonColor
                  : this.context.buttonColor,

                padding: 10,
                opacity: pressed ? 0.4 : 1,
                width: "45%",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 4,
              },
            ]}
          >
            <Text
              style={{
                color: this.context.buttonTextColor,
                fontWeight: "bold",
              }}
            >
              {item.localizedPrice}
            </Text>
          </Pressable>
        </View>
      </View>
    );
  };

  componentDidMount = async () => {
    this.appStateSubscription = AppState.addEventListener(
      "change",
      (nextAppState) => {
        if (
          this.state.appState.match(/background/) &&
          nextAppState === "active"
        ) {
          this.props.navigation.pop();
        }
        this.setState({ appState: nextAppState });
      }
    );
    //_parentProducts and _childProducts are the props that bring products related with the parent package and the serie you land in
    const { _parentPayments, _childSubscription, _childProduct } =
      this.props.route.params;

    await IAP.initConnection().then(async () => {
      try {
        if (!/^\s*$/.test(_childSubscription)) {
          const childSubscription: Subscription[] = await IAP.getSubscriptions([
            _childSubscription,
          ]);
          this.setState({ childSubscription });
        }
      } catch (err) {
        console.warn(err); // standardized err.code and err.message available
      }
      try {
        if (!/^\s*$/.test(_childProduct)) {
          const childProduct: Product[] = await IAP.getProducts([
            _childProduct,
          ]);
          this.setState({ childProduct });
        }
      } catch (err) {
        console.warn(err); // standardized err.code and err.message available
      }
      try {
        if (_parentPayments.length) {
          const parentSubsIds: string[] = _parentPayments
            .filter((item: Parent) => {
              return !/^\s*$/.test(item.subscription);
            })
            .map((item: Parent) => {
              return item.subscription;
            });
          if (parentSubsIds.length > 0) {
            const parentSubscriptions: Subscription[] =
              await IAP.getSubscriptions(parentSubsIds);
            this.setState({ parentSubscriptions });
          }
        }
      } catch (err) {
        console.warn(err); // standardized err.code and err.message available
      }

      try {
        if (_parentPayments.length) {
          const parentProdsIds: string[] = _parentPayments
            .filter((item: Parent) => {
              return !/^\s*$/.test(item.product);
            })
            .map((item: Parent) => {
              return item.product;
            });
          if (parentProdsIds.length > 0) {
            const parentProducts: Product[] = await IAP.getProducts(
              parentProdsIds
            );
            this.setState({ parentProducts });
          }
        }
      } catch (err) {
        console.warn(err); // standardized err.code and err.message available
      }
      this.setState({ loading: false });
      this.purchaseUpdateSubscription = purchaseUpdatedListener(
        async (purchase: SubscriptionPurchase | ProductPurchase) => {
          const { id_series } = this.props.route.params;
          const bundleID = getBundleId();
          const parentId = this.parentId.get("id");
          const receipt: any =
            Platform.OS === "ios"
              ? purchase.transactionReceipt
              : Platform.OS === "android"
              ? JSON.parse(purchase.transactionReceipt)
              : null;

          if (receipt) {
            if (Platform.OS === "ios") {
              this.validate_IOS(receipt, parentId ? parentId : id_series);
            } else if (Platform.OS === "android") {
              this.validate_ANDROID(
                receipt["productId"],
                receipt["purchaseToken"],
                bundleID,
                parentId ? parentId : id_series
              );
            }
            IAP.finishTransaction(purchase);
          }
        }
      );

      this.purchaseErrorSubscription = purchaseErrorListener(
        (error: PurchaseError) => {
          console.warn("purchaseErrorListener", error);
          this.setState({ loading: false });
        }
      );
    });
  };
  componentWillUnmount() {
    AppState.removeEventListener("change", (nextAppState) => {
      if (
        this.state.appState.match(/background/) &&
        nextAppState === "active"
      ) {
        this.props.navigation.pop();
      }
      this.setState({ appState: nextAppState });
    });
    if (this.purchaseUpdateSubscription) {
      this.purchaseUpdateSubscription.remove();
      this.purchaseUpdateSubscription = null;
    }
    if (this.purchaseErrorSubscription) {
      this.purchaseErrorSubscription.remove();
      this.purchaseErrorSubscription = null;
    }
    IAP.endConnection();
  }
  renderResult() {
    if (
      this.state.loading &&
      !this.state.paymentSuccessfull &&
      !this.state.paymentFailed
    ) {
      return (
        <View
          style={[
            { backgroundColor: this.context.backgroundColor },
            styles.container,
          ]}
        >
          <OrientationLocker orientation={PORTRAIT} />
          <ActivityIndicator
            size="large"
            color={this.context.buttonColor || "black"}
          />
        </View>
      );
    } else {
      if (this.state.paymentSuccessfull && !this.state.paymentFailed) {
        return (
          <View
            style={[
              { backgroundColor: this.context.backgroundColor },
              styles.container,
              { flexDirection: "column" },
            ]}
          >
            <View
              style={{
                marginBottom: 15,
              }}
            >
              <FontAwesomeIcon icon={faCheck} size={105} color={"#00FF00"} />
            </View>

            <Text
              style={{
                color: this.context.fontColor,
                fontSize: 20,
                fontWeight: "bold",
                marginBottom: 10,
                textAlign: "center",
              }}
            >
              {global.strings.PaymentSuccess}
            </Text>

            <Text
              style={{
                color: this.context.fontColor,
                textAlign: "center",
              }}
            >
              {global.strings.PaymentRedirect}
            </Text>
          </View>
        );
      } else {
        return (
          <View
            style={[
              { backgroundColor: this.context.backgroundColor },
              styles.container,
              { flexDirection: "column" },
            ]}
          >
            <View
              style={{
                marginBottom: 15,
              }}
            >
              <FontAwesomeIcon icon={faTimes} size={105} color={"#cc0000"} />
            </View>

            <Text
              style={{
                color: this.context.fontColor,
                fontSize: 20,
                fontWeight: "bold",
                marginBottom: 10,
                textAlign: "center",
              }}
            >
              {global.strings.PaymentFailed}
            </Text>

            <Text
              style={{
                color: this.context.fontColor,
                textAlign: "center",
              }}
            >
              {global.strings.PaymentRedirect}
            </Text>
          </View>
        );
      }
    }
  }

  render(): React.ReactNode {
    return !this.state.loading &&
      !this.state.paymentSuccessfull &&
      !this.state.paymentFailed ? (
      this.state.parentSubscriptions.length > 0 ||
      this.state.childProduct ||
      this.state.childSubscription ||
      this.state.parentProducts ? (
        <View
          style={[
            { backgroundColor: this.context.backgroundColor },
            styles.container,
          ]}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              width: "100%",
              height: "100%",
            }}
          >
            <OrientationLocker orientation={PORTRAIT} />
            <View
              style={{
                borderBottomColor: this.context.fontColor,
                borderStyle: "solid",
                borderBottomWidth: 0.5,
                padding: 5,
                width: "90%",
                height: "100%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text
                style={[
                  styles.mainHeader,
                  {
                    color: this.context.fontColor,
                  },
                ]}
              >
                {global.strings.ChoosePlan}
              </Text>
            </View>
          </View>
          <View style={{ flex: 5 }}>
            <FlatList
              style={styles.flatList}
              data={[
                ...this.state.childSubscription,
                ...this.state.childProduct,
                ...this.state.parentSubscriptions,
                ...this.state.parentProducts,
              ]}
              renderItem={this.renderItem}
              keyExtractor={(item) => item.productId}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              marginBottom: 12,
            }}
          >
            <View>
              <Text
                style={{
                  color: this.context.fontColor,
                  textDecorationLine: "underline",
                  fontWeight: "bold",
                  fontSize: 8,
                }}
                onPress={() => this.openTerms()}
              >
                {global.strings.Terms}
              </Text>
            </View>
            <View>
              <Text
                style={{
                  color: this.context.fontColor,
                  fontSize: 8,
                  marginHorizontal: 5,
                }}
              >
                &
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 8,
                  color: this.context.fontColor,
                  textDecorationLine: "underline",
                }}
                onPress={() => this.openPrivacyPolicy()}
              >
                {global.strings.PrivacyPolicy}
              </Text>
            </View>
          </View>
        </View>
      ) : (
        <View
          style={[
            { backgroundColor: this.context.backgroundColor },
            styles.container,
          ]}
        >
          <OrientationLocker orientation={PORTRAIT} />
          <ActivityIndicator
            size="large"
            color={this.context.buttonColor || "black"}
          />
        </View>
      )
    ) : (
      this.renderResult()
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 22,
    color: "white",
  },
  mainHeader: {
    fontSize: 25,
    fontWeight: "bold",
  },
  flatList: { width: "100%", marginTop: 20 },
  productsContainer: {
    borderRadius: 4,
    justifyContent: "center",
    marginVertical: 8,
    marginHorizontal: 16,
    padding: 20,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
    elevation: 15,
  },
});
export { InAppView };
