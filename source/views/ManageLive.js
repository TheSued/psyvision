import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  ImageBackground,
  BackHandler,
  Platform,
} from 'react-native';

import { check, request, PERMISSIONS, RESULTS } from 'react-native-permissions';
import LinearGradient from 'react-native-linear-gradient';

import { Header, Toast } from '../components';
import { Background, AppText, Touchable } from '../components/common';
import { ActiveCompanyContext } from '../providers/CompanyProvider';
import * as Network from '../network';

import Routes from '../environments/Routes';
import Colors from '../environments/Colors';

const currentScreen = 'ManageLive';

class ManageLive extends React.Component {
  static contextType = ActiveCompanyContext;
  state = {
    name: '',
    image: '',
    scheduledStep: 1,
    scheduled: [],
    loading: true,
    logoutRequest: false,
    listEndReached: false,
    companies: [],
    cameraPermission: false,
    microphonePermission: false,
    errorMessage: '',
    pageHasFocus: false,
  };

  async checkPermission() {
    let response;
    if (Platform.OS == 'android') {
      response = await Promise.all([check(PERMISSIONS.ANDROID.CAMERA), check(PERMISSIONS.ANDROID.RECORD_AUDIO)]);
    } else {
      response = await Promise.all([check(PERMISSIONS.IOS.CAMERA), check(PERMISSIONS.IOS.MICROPHONE)]);
    }
    this.setState({
      cameraPermission: response[0] == RESULTS.GRANTED ? true : false,
      microphonePermission: response[1] == RESULTS.GRANTED ? true : false,
    });
  }

  async getScheduled() {
    const liveData = await Network.getScheduledLives(this.state.scheduledStep);
    this.setState({ scheduled: liveData });
    if (liveData.lenght == global.videoEachStep) {
      this.setState({ scheduledStep: this.state.scheduledStep + 1 });
    }
    this.setState({ loading: false });
    /* set timer to fetch scheduled live every 15 seconds */
    this.scheduledTimer = setInterval(() => this.timedFetchScheduled(), 15000);
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('focus', () => this.viewWillFocus());
    this.blurListener = this.props.navigation.addListener('blur', () => this.viewWillBlur());
    const { params } = this.props.route;

    this.setState({ image: params.image, name: params.name });

    /* retrieve scheduled lives */
    this.getScheduled();
    this.checkPermission();
  }

  viewWillFocus() {
    this.setState({ pageHasFocus: true });
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => this.goBack());
  }

  componentWillUnmount() {
    if (this.backHandler) {
      this.backHandler.remove();
    }
    clearInterval(this.scheduledTimer);
    this.focusListener();
    this.blurListener();
  }

  viewWillBlur() {
    this.setState({ pageHasFocus: false });

    if (this.backHandler) {
      this.backHandler.remove();
    }
    clearInterval(this.scheduledTimer);
  }

  async goBack() {
    const { previousScreen } = this.props.route.params;
    this.props.navigation.goBack();
  }

  async timedFetchScheduled() {
    if (
      this.state.scheduled != undefined &&
      this.state.scheduled.length != undefined &&
      this.state.scheduled.length != 0
    ) {
      const timedData = await Network.getScheduledLives(1);
      if (timedData != 0 && timedData.length != 0 && timedData[0] != this.state.scheduled[0]) {
        this.setState({ scheduled: timedData });
        if (timedData.length == global.videoEachStep) {
          this.setState({ scheduledStep: 2 });
        } else {
          this.setState({ scheduledStep: 1 });
        }
      }
    }
  }

  async fetchScheduled() {
    if (this.state.scheduledStep != 1) {
      const data = await Network.getScheduledLives(this.state.scheduledStep);
      if (data.length) {
        this.setState({ scheduled: [...this.state.scheduled, ...responseJson] });
        if (data.length == global.videoEachStep) {
          this.setState({ scheduledStep: this.state.scheduledStep + 1 });
        }
      }
    }
  }

  async askMicrophonePermissions() {
    let response = 0;
    if (Platform.OS == 'android') {
      response = await request(PERMISSIONS.ANDROID.RECORD_AUDIO);
    } else {
      response = await request(PERMISSIONS.IOS.MICROPHONE);
    }
    if (response == RESULTS.GRANTED) {
      this.setState({ microphonePermission: true });
      return true;
    } else {
      this.setState({ microphonePermission: false });
      return false;
    }
  }

  async askCameraPermissions() {
    let response = 0;
    if (Platform.OS == 'android') {
      response = await request(PERMISSIONS.ANDROID.CAMERA);
    } else {
      response = await request(PERMISSIONS.IOS.CAMERA);
    }
    if (response == RESULTS.GRANTED) {
      this.setState({ cameraPermission: true });
      return true;
    } else {
      this.setState({ cameraPermission: false });
      return false;
    }
  }

  async startScheduled(item) {
    const { cameraPermission, microphonePermission } = this.state;
    let camera;
    let microphone;
    if (item != null && item.serverlive != null && item.code != null && item.id != null && item.stream_key != null) {
      if (!cameraPermission) {
        camera = await this.askCameraPermissions();
      }
      if (!microphonePermission) {
        microphone = await this.askMicrophonePermissions();
      }
      if ((cameraPermission || camera) && (microphonePermission || microphone)) {
        this.props.navigation.navigate('StreamView', {
          previousScreen: currentScreen,
          streamId: item.id,
          streamData: {
            serverLive: item.serverlive,
            streamKey: item.stream_key,
            streamCode: item.code,
          },
        });
      } else {
        this.setState({ errorMessage: global.strings.StreamPermissions }, () => {
          setTimeout(() => {
            this.setState({ errorMessage: '' });
          }, 3000);
        });
      }
    } else {
      this.setState({ errorMessage: global.strings.GeneralError }, () => {
        setTimeout(() => this.setState({ errorMessage: '' }), 3000);
      });
    }
  }

  render() {
    if (this.state.pageHasFocus) {
      const { errorMessage } = this.state;

      return (
        <View style={{ flex: 1 }}>
          <Background
            simple
            header={
              <Header
                onMenuPress={() => this.goBack()}
                onLogoPress={() => this.props.navigation.popToTop()}
                onAddPress={() =>
                  this.props.navigation.navigate('AddLive', {
                    previousScreen: currentScreen,
                  })
                }
              />
            }>
            {/* scheduled list */}
            <FlatList
              data={this.state.scheduled}
              renderItem={({ item }) => (
                <Touchable style={{ margin: 10 }} onPress={() => this.startScheduled(item)}>
                  <ImageBackground
                    imageStyle={{ borderRadius: 5 }}
                    source={{ uri: item.img_preview || this.context.activeCompany.default_video_image }}
                    style={[
                      styles.livePreviewStyle,
                      {
                        width: global.screenProp.width - 20,
                        height: ((global.screenProp.width - 20) / 16) * 9,
                      },
                    ]}>
                    <LinearGradient
                      colors={['black', 'transparent']}
                      start={{ x: 0.2, y: 1.0 }}
                      end={{ x: 0.2, y: 0.0 }}
                      style={[styles.translucentStyle, { height: ((global.screenProp.width - 20) / 16) * 9 }]}>
                      <AppText style={styles.liveTitleStyle}>{item.title}</AppText>
                      <AppText style={styles.liveCollectionStyle}>{item.date || ''}</AppText>
                    </LinearGradient>
                  </ImageBackground>
                </Touchable>
              )}
              onEndReached={() => this.fetchScheduled()}
              onEndReachedThreshold={0.1}
              keyExtractor={(item) => String(item.id)}
              style={styles.flex}
            />
          </Background>
          <Toast message={errorMessage} />
        </View>
      );
    } else {
      return <Background empty />;
    }
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  liveDataContainerStyle: {
    alignItems: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    elevation: 1,
    marginLeft: 8,
    marginRight: 8,
  },
  livePreviewStyle: {
    height: 163,
    width: 290,
    borderRadius: 5,
  },
  liveTitleStyle: {
    fontSize: 18,
    fontWeight: '600',
  },
  liveCollectionStyle: {
    color: Colors.PlaceholderText,
    fontSize: 14,
  },
  liveDetailsContainerStyle: {
    justifyContent: 'center',
    paddingLeft: 5,
  },
  liveDateStyle: {
    color: Colors.PlaceholderText,
    marginBottom: 2,
    marginLeft: 3,
    fontSize: 14,
  },
  noLiveTextStyle: {
    color: Colors.Text1,
    marginLeft: 3,
    fontSize: 16,
  },
  spinnerContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  // footer
  buttonIconContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonGradientStyle: {
    // position: 'absolute',
    height: 80,
    alignSelf: 'stretch',
    elevation: 1,
  },
  buttonIconStyle: {
    color: Colors.Text1,
    fontWeight: '600',
    alignSelf: 'center',
  },
  buttonStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  translucentStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 8,
  },
});

export { ManageLive };
