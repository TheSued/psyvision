"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.SerieView = void 0;
var react_1 = require("react");
var react_native_1 = require("react-native");
var react_native_fontawesome_1 = require("@fortawesome/react-native-fontawesome");
var free_solid_svg_icons_1 = require("@fortawesome/free-solid-svg-icons");
var react_native_linear_gradient_1 = require("react-native-linear-gradient");
var react_native_video_1 = require("react-native-video");
var react_native_indicators_1 = require("react-native-indicators");
var Network = require("../network");
var Colors_1 = require("../environments/Colors");
var common_1 = require("../components/common");
var components_1 = require("../components");
var CompanyProvider_1 = require("../providers/CompanyProvider");
var common_utils_1 = require("../utils/common.utils");
var currentScreen = "SerieView";
var SerieView = /** @class */ (function (_super) {
    __extends(SerieView, _super);
    function SerieView() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            access: false,
            trailerVolumeMuted: true,
            // serie
            currentSerie: {
                id: null,
                data: [],
                step: 1,
                loading: false
            },
            // trailer: false,
            errorMessage: "",
            packSeries: "",
            // switch between watch trailer and normal SerieView
            fullscreen: false,
            // trailer states
            screenHeight: (global.screenProp.width / 16) * 9,
            screenWidth: global.screenProp.width - 6,
            trailerSource: null,
            muted: true,
            currentTime: 0,
            secondsPlayed: 0,
            intervalId: 0,
            updateIntervalId: 0,
            bundleInfo: [],
            // these control the render of the pay buttons
            paySerie: false,
            paySubscription: false,
            subscription: {
                period: 0,
                price: 0
            },
            subscriptionPrice: 0,
            subscriptionPeriod: 0,
            pageHasFocus: false,
            elementPreviewMarker: [],
            elementShowPreview: []
        };
        // TODO: misleading fullscreen or trailer?
        _this.end = 0;
        _this.action = "";
        _this.trailerVolumeMuted = true;
        _this.focusListener = null;
        _this.blurListener = null;
        _this.onProgress = function (data) {
            _this.setState({ currentTime: data.currentTime });
        };
        _this.onLoad = function () {
            _this.setAction();
            _this.counter();
            _this.setUpdateInterval();
        };
        return _this;
    }
    SerieView.prototype.getTrailer = function (trailer) {
        return __awaiter(this, void 0, void 0, function () {
            var videoInfo, video;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Network.getVideoSource(trailer)];
                    case 1:
                        videoInfo = _a.sent();
                        if (videoInfo) {
                            video = common_utils_1.formatVideoObject(videoInfo);
                            this.setState({
                                trailerSource: video.fullhd || video.hd || video.hq || video.original
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    SerieView.prototype.getBundleChild = function (serieId) {
        return __awaiter(this, void 0, void 0, function () {
            var bundleInfo, step, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        bundleInfo = this.state.bundleInfo;
                        step = 0;
                        if (bundleInfo.length == 0) {
                            step = 1; // first load
                        }
                        else if (bundleInfo.length % global.videoEachStep == 0) {
                            step = bundleInfo.length / global.videoEachStep + 1; // if length == videoEachStep then load the next array
                        }
                        if (!(step != 0)) return [3 /*break*/, 2];
                        return [4 /*yield*/, Network.getBundleChild(serieId, step)];
                    case 1:
                        data = _a.sent();
                        if (data && data.length) {
                            this.setState({ bundleInfo: __spreadArrays(bundleInfo, data) });
                        }
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    SerieView.prototype.getSerieParent = function (serieId) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Network.getSerieParent(serieId)];
                    case 1:
                        data = _a.sent();
                        if (data && data.length) {
                            this.setState({ bundleInfo: data });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    SerieView.prototype.navigateToPayment = function (paymentData) {
        var serie = this.props.route.params.serie;
        if (this.context.user) {
            this.props.navigation.navigate("Payment", {
                serie: serie,
                previousScreen: currentScreen,
                payment: paymentData.period,
                price: paymentData.price
            });
        }
        else {
            this.props.navigation.navigate("Login", {
                previousScreen: currentScreen
            });
        }
    };
    SerieView.prototype.navigateToPaymentSerie = function (paymentData) {
        var serie = this.props.route.params.serie;
        if (this.context.user) {
            this.props.navigation.navigate("PaymentSerie", {
                serie: serie.name,
                previousScreen: "currentScreen",
                payment: paymentData.period,
                price: paymentData.price
            });
        }
        else {
            this.props.navigation.navigate("Login", {
                previousScreen: currentScreen
            });
        }
    };
    SerieView.prototype.getSerieVideo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var serie, currentSerie, data, step;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        serie = this.props.route.params.serie;
                        currentSerie = this.state.currentSerie;
                        return [4 /*yield*/, Network.getSerieVideo(this.context.activeCompany.id, serie.id, currentSerie.step)];
                    case 1:
                        data = _a.sent();
                        if (data) {
                            if (data.length == global.videoEachStep) {
                                step = currentSerie.step + 1;
                            }
                            else {
                                step = currentSerie.step;
                            }
                        }
                        this.setState({
                            currentSerie: { id: serie.id, data: data, step: step, loading: false }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    SerieView.prototype.refresh = function () {
        return __awaiter(this, void 0, void 0, function () {
            var serie, currentSerie, pSerie, pSubscription, subscription;
            return __generator(this, function (_a) {
                serie = this.props.route.params.serie;
                currentSerie = this.state.currentSerie;
                this.setState({
                    currentSerie: { data: [], loading: true, id: serie.id, step: 1 },
                    access: serie.access == "ok" ? true : false,
                    paySerie: false,
                    paySubscription: false,
                    bundleInfo: []
                });
                if (serie.trailer) {
                    this.getTrailer(serie.trailer);
                }
                if (serie.parent) {
                    this.getBundleChild(serie.id);
                }
                else if (serie.access != "ok") {
                    this.getSerieParent(serie.id);
                }
                if (serie && serie.access !== "ok") {
                    pSerie = false;
                    pSubscription = false;
                    subscription = {
                        price: 0,
                        period: 0
                    };
                    if (serie.payment.price) {
                        pSerie = true;
                    }
                    if (serie.payment.subscription.length) {
                        pSubscription = true;
                        subscription = {
                            period: serie.payment.subscription[0].period,
                            price: serie.payment.subscription[0].price
                        };
                    }
                    this.setState({
                        currentSerie: __assign(__assign({}, currentSerie), { loading: false }),
                        paySerie: pSerie,
                        paySubscription: pSubscription,
                        subscription: subscription
                    });
                }
                else {
                    // show serie videos
                    this.getSerieVideo();
                }
                return [2 /*return*/];
            });
        });
    };
    SerieView.prototype.viewWillFocus = function () {
        var _this = this;
        this.setState({ pageHasFocus: true });
        if (this.props.route.params.serie.id != this.state.currentSerie.id) {
            this.refresh();
        }
        this.backHandler = react_native_1.BackHandler.addEventListener("hardwareBackPress", function () {
            return _this.goBack();
        });
    };
    SerieView.prototype.goBack = function () {
        return __awaiter(this, void 0, void 0, function () {
            var previousScreen, fullscreen;
            return __generator(this, function (_a) {
                previousScreen = this.props.route.params.previousScreen;
                fullscreen = this.state.fullscreen;
                if (previousScreen == "SerieView") {
                    // this is the case we are in a pushed SerieView
                    this.props.navigation.goBack(null);
                }
                else {
                    // this is the case this SerieView is the first
                    // and we are going to the Catalog
                    this.props.navigation.goBack(null);
                }
                return [2 /*return*/];
            });
        });
    };
    SerieView.prototype.viewWillBlur = function () {
        this.setState({ pageHasFocus: false });
        if (this.backHandler) {
            this.backHandler.remove();
        }
    };
    SerieView.prototype.componentWillUnmount = function () {
        if (this.backHandler) {
            this.backHandler.remove();
        }
        this.focusListener();
        this.blurListener();
    };
    SerieView.prototype.componentWillMount = function () {
        this.overlayOpacity = new react_native_1.Animated.Value(1);
    };
    SerieView.prototype.componentDidMount = function () {
        var _this = this;
        var serie = this.props.route.params.serie;
        console.log("questo console lo", serie.app_products);
        this.focusListener = this.props.navigation
            .addListener("focus", function () {
            _this.end = 0;
            _this.viewWillFocus();
        })
            .bind(this);
        this.blurListener = this.props.navigation
            .addListener("blur", function () {
            _this.end = 1;
            _this.updateVideo();
            _this.stopCounter();
            _this.stopUpdateInterval();
            _this.viewWillBlur();
        })
            .bind(this);
    };
    SerieView.prototype.openLink = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.props.navigation.navigate("WebViewPage", {
                    source: "https://teyuto.tv/app/paymentPreview?idc=1577&app=1",
                    headers: {
                    // Authorization: await Storage.getToken()
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    SerieView.prototype.fetchMore = function () {
        return __awaiter(this, void 0, void 0, function () {
            var serie, currentSerie, data, step;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        serie = this.props.route.params.serie;
                        currentSerie = this.state.currentSerie;
                        if (!(currentSerie.data.length % global.videoEachStep == 0)) return [3 /*break*/, 2];
                        return [4 /*yield*/, Network.getSerieVideo(this.context.activeCompany.id, serie.id, currentSerie.step)];
                    case 1:
                        data = _a.sent();
                        if (data && data.length) {
                            step = void 0;
                            if (data.length == global.videoEachStep) {
                                step = currentSerie.step + 1;
                            }
                            else {
                                step = currentSerie.step;
                            }
                            this.setState({
                                currentSerie: __assign(__assign({}, currentSerie), { data: __spreadArrays(currentSerie.data, data), step: step, loading: false })
                            });
                        }
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    SerieView.prototype.onError = function (error) {
        var _this = this;
        this.setState({ errorMessage: error }, function () {
            setTimeout(function () { return _this.setState({ errorMessage: "" }); }, 1800);
        });
    };
    SerieView.prototype.openSeries = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            var serie, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        serie = this.props.route.params.serie;
                        this.setState({
                            currentSerie: __assign(__assign({}, this.state.currentSerie), { loading: true })
                        });
                        if (!serie.parent) return [3 /*break*/, 2];
                        return [4 /*yield*/, Network.getSerieInfo(item.id_child)];
                    case 1:
                        data = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, Network.getSerieInfo(item.id_parent)];
                    case 3:
                        data = _a.sent();
                        _a.label = 4;
                    case 4:
                        this.setState({
                            currentSerie: __assign(__assign({}, this.state.currentSerie), { loading: false })
                        });
                        if (data && data[0]) {
                            this.props.navigation.push("SerieView", {
                                serie: data[0],
                                previousScreen: currentScreen
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    SerieView.prototype.openPlayer = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            var currentSerie, promises, videoInfo, data, attachments, video;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        currentSerie = this.state.currentSerie;
                        if (!item.quiz) return [3 /*break*/, 1];
                        this.onError(global.strings.QuizAlert);
                        return [3 /*break*/, 3];
                    case 1:
                        this.context.setActualPreview(item.img_preview);
                        // use the same state of the serie for a loading feedback
                        this.setState({ currentSerie: __assign(__assign({}, currentSerie), { loading: true }) });
                        return [4 /*yield*/, Promise.all([
                                Network.getVideoInfo(item.id),
                                Network.getVideoSource(item.id),
                                Network.getAttachments(item.id),
                            ])];
                    case 2:
                        promises = _a.sent();
                        videoInfo = promises[0];
                        data = promises[1];
                        attachments = promises[2];
                        video = common_utils_1.formatVideoObject(data);
                        if (!videoInfo.ondemand) {
                            video.started = videoInfo.livenow;
                        }
                        // use the same state of the serie for a loading feedback
                        this.setState({ currentSerie: __assign(__assign({}, currentSerie), { loading: false }) });
                        this.props.navigation.navigate("Player", {
                            previousScreen: currentScreen,
                            video: {
                                id: item.id,
                                creator: item.display_name,
                                title: item.title,
                                description: item.description,
                                source: video,
                                sourceChromecast: data.source_chromecast,
                                sourceMP4: data.source_original,
                                attachments: attachments
                            },
                            videoInfo: {
                                showLike: videoInfo.show_likes,
                                showViews: videoInfo.show_views,
                                likesTot: videoInfo.likes_tot,
                                viewsTot: videoInfo.views,
                                liked: !!videoInfo.liked,
                                markers: videoInfo.markers,
                                productRelated: videoInfo.related_product
                            }
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SerieView.prototype.setAction = function () {
        return __awaiter(this, void 0, void 0, function () {
            var serie, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        serie = this.props.route.params.serie;
                        _a = this;
                        return [4 /*yield*/, Network.playVideo(serie.trailer, 0)];
                    case 1:
                        _a.action = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SerieView.prototype.updateVideo = function (time) {
        return __awaiter(this, void 0, void 0, function () {
            var serie;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        serie = this.props.route.params.serie;
                        return [4 /*yield*/, Network.updateVideo(serie.trailer, time || this.state.currentTime.toFixed(2) || 0, this.action, this.end, this.state.secondsPlayed)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SerieView.prototype.counter = function () {
        var _this = this;
        var intervalId = setInterval(function () {
            _this.setState({ secondsPlayed: _this.state.secondsPlayed + 1 });
        }, 1000);
        this.setState({ intervalId: intervalId });
    };
    SerieView.prototype.stopCounter = function () {
        clearInterval(this.state.intervalId);
        this.setState({ secondsPlayed: 0 });
    };
    SerieView.prototype.setUpdateInterval = function () {
        var _this = this;
        var updateIntervalId = setInterval(function () {
            _this.updateVideo();
            _this.setState({ secondsPlayed: 0 });
        }, 20000);
        this.setState({ updateIntervalId: updateIntervalId });
    };
    SerieView.prototype.stopUpdateInterval = function () {
        clearInterval(this.state.updateIntervalId);
    };
    SerieView.prototype.render = function () {
        var _this = this;
        if (this.state.pageHasFocus) {
            var serie_1 = this.props.route.params.serie;
            var _a = this.state, currentSerie = _a.currentSerie, fullscreen = _a.fullscreen;
            return (react_1["default"].createElement(common_1.Background, { header: react_1["default"].createElement(components_1.Header, { onMenuPress: function () { return _this.props.navigation.goBack(null); }, onLogoPress: function () { return _this.props.navigation.popToTop(); } }), style: {
                    backgroundColor: this.context.backgroundColor
                } },
                react_1["default"].createElement(react_native_1.ScrollView, { style: {
                        flex: 6,
                        backgroundColor: this.context.backgroundColor
                    }, nestedScrollEnabled: true, showsVerticalScrollIndicator: false },
                    react_1["default"].createElement(react_native_1.View, { style: {
                            width: global.screenProp.width,
                            height: (global.screenProp.width / 16) * 9,
                            position: "relative",
                            flexDirection: "column",
                            backgroundColor: this.context.backgroundColor,
                            justifyContent: "flex-end"
                        } },
                        react_1["default"].createElement(OrientationLocker, { orientation: PORTRAIT }),
                        this.state.trailerSource && (react_1["default"].createElement(react_native_video_1["default"], { source: {
                                uri: this.state.trailerSource === null
                                    ? null
                                    : this.state.trailerSource
                            }, muted: this.state.trailerVolumeMuted, onProgress: this.onProgress, onLoad: this.onLoad, playbackRate: 1, repeat: true, resizeMode: "cover", rate: 1.0, poster: serie_1.img_preview, ignoreSilentSwitch: "ignore", ref: function (ref) { return (_this.video = ref); }, style: {
                                position: "absolute",
                                top: 0,
                                bottom: 0,
                                left: 0,
                                right: 0
                            } })),
                        react_1["default"].createElement(react_native_linear_gradient_1["default"], { colors: [
                                this.context.backgroundColor + "00",
                                this.context.backgroundColor + "00",
                                this.context.backgroundColor + "00",
                                this.context.backgroundColor,
                            ], style: {
                                width: "100%",
                                flexDirection: "row",
                                alignItems: "center"
                            } },
                            react_1["default"].createElement(react_native_1.View, { style: {
                                    padding: 7
                                } }, this.state.trailerVolumeMuted ? (react_1["default"].createElement(react_native_1.TouchableWithoutFeedback, { onPress: function () {
                                    return _this.setState({ trailerVolumeMuted: false });
                                } },
                                react_1["default"].createElement(react_native_1.View, null,
                                    react_1["default"].createElement(react_native_fontawesome_1.FontAwesomeIcon, { icon: free_solid_svg_icons_1.faVolumeMute, size: 25, color: this.context.fontColor })))) : (react_1["default"].createElement(react_native_1.TouchableWithoutFeedback, { onPress: function () {
                                    return _this.setState({ trailerVolumeMuted: true });
                                } },
                                react_1["default"].createElement(react_native_1.View, null,
                                    react_1["default"].createElement(react_native_fontawesome_1.FontAwesomeIcon, { icon: free_solid_svg_icons_1.faVolumeUp, size: 25, color: this.context.fontColor }))))),
                            react_1["default"].createElement(react_native_1.View, { style: {
                                    padding: 7
                                } },
                                react_1["default"].createElement(react_native_1.TouchableWithoutFeedback, { onPress: function () {
                                        if (_this.state.trailerSource) {
                                            _this.video.seek(0);
                                        }
                                    } },
                                    react_1["default"].createElement(react_native_1.View, null,
                                        react_1["default"].createElement(react_native_fontawesome_1.FontAwesomeIcon, { icon: free_solid_svg_icons_1.faRedo, size: 25, color: this.context.fontColor })))))),
                    react_1["default"].createElement(react_native_1.View, { style: { flexDirection: "column" } },
                        react_1["default"].createElement(react_native_1.View, null,
                            react_1["default"].createElement(react_native_1.View, null,
                                react_1["default"].createElement(common_1.AppText, { style: {
                                        fontSize: 20,
                                        fontWeight: "600",
                                        paddingLeft: 10,
                                        marginTop: 7
                                    } }, serie_1.title)),
                            react_1["default"].createElement(react_native_1.View, null,
                                react_1["default"].createElement(common_1.AppText, { style: {
                                        color: "rgb(129, 129, 130)",
                                        fontSize: 16,
                                        fontWeight: "300",
                                        paddingLeft: 10,
                                        marginBottom: 20,
                                        marginTop: 20,
                                        lineHeight: 21
                                    } }, serie_1.description)),
                            serie_1.child && (react_1["default"].createElement(components_1.HomePageList, { title: global.strings.SerieIncluded, data: bundleInfo, onRef: function (ref) { return (_this.onItemPress = ref); }, onItemPress: this.openSeries.bind(this), onEndReached: function () { return _this.getBundleChild(); }, onEndReachedThreshold: 0.5, style: { width: "100%" } })),
                            serie_1.parent && (react_1["default"].createElement(components_1.HomePageList, { title: global.strings.BundleContaining, data: bundleInfo, onRef: function (ref) { return (_this.onItemPress = ref); }, onItemPress: this.openSeries.bind(this), style: { width: "100%" } }))),
                        serie_1.access == "ok" && (react_1["default"].createElement(react_native_1.View, null,
                            react_1["default"].createElement(react_native_1.FlatList, { data: currentSerie.data, style: {
                                    marginBottom: 20
                                }, renderItem: function (_a) {
                                    var item = _a.item;
                                    return (react_1["default"].createElement(react_native_1.View, { style: { marginBottom: 22 } },
                                        react_1["default"].createElement(common_1.Touchable, { style: {
                                                margin: 10,
                                                flexDirection: "column"
                                            }, onPress: function () { return _this.openPlayer(item); } },
                                            react_1["default"].createElement(react_native_1.ImageBackground, { imageStyle: { borderRadius: 5 }, source: {
                                                    uri: item.img_preview ||
                                                        _this.context.activeCompany.default_video_image
                                                }, style: [
                                                    styles.livePreviewStyle,
                                                    {
                                                        width: global.screenProp.width - 20,
                                                        height: ((global.screenProp.width - 20) / 16) * 9
                                                    },
                                                ] }, item.duration ? (react_1["default"].createElement(common_1.AppText, { style: styles.liveCollectionStyle }, item.duration)) : (react_1["default"].createElement(common_1.AppText, null)))),
                                        react_1["default"].createElement(common_1.AppText, { style: styles.liveTitleStyle }, item.title)));
                                }, onEndReached: function () { return _this.fetchMore(); }, keyExtractor: function (item) { return String(item.id); }, style: styles.flex, onEndReachedThreshold: 0.1, ListFooterComponent: react_1["default"].createElement(react_native_1.View, { style: { height: 50 } }) }))),
                        !fullscreen && serie_1.access != "ok" && (react_1["default"].createElement(react_native_1.View, { style: { justifyContent: "center", alignItems: "center" } }, (serie_1.app_products && serie_1.app_products.product) ||
                            (serie_1.app_products && serie_1.app_products.subscription) ||
                            (serie_1.app_products && serie_1.app_products.included.length) ? (react_1["default"].createElement(react_native_1.Pressable, { onPress: function () {
                                _this.props.navigation.navigate("ModalPay", {
                                    _childProduct: serie_1.app_products.product,
                                    _childSubscription: serie_1.app_products.subscription,
                                    _parentPayments: serie_1.app_products.included,
                                    id_series: serie_1.id
                                });
                            }, style: function (_a) {
                                var pressed = _a.pressed;
                                return [
                                    {
                                        width: "85%",
                                        backgroundColor: _this.context.buttonColor,
                                        alignItems: "center",
                                        justifyContent: "center",
                                        padding: 10,
                                        opacity: pressed ? 0.4 : 1,
                                        borderRadius: 4
                                    },
                                ];
                            } }, function (_a) {
                            var pressed = _a.pressed;
                            return (react_1["default"].createElement(react_native_1.View, { style: {
                                    flex: 1,
                                    alignItems: "center",
                                    justifyContent: "space-around",
                                    flexDirection: "row"
                                } },
                                react_1["default"].createElement(react_native_1.View, { style: {
                                        flex: 5,
                                        justifyContent: "center",
                                        alignItems: "center"
                                    } },
                                    react_1["default"].createElement(react_native_1.Text, { style: {
                                            color: _this.context.fontColor,
                                            fontWeight: "bold",
                                            fontSize: 24
                                        } }, global.strings.AccessContent)),
                                react_1["default"].createElement(react_native_1.View, { style: {
                                        flex: 1,
                                        justifyContent: "center",
                                        alignItems: "center"
                                    } },
                                    react_1["default"].createElement(react_native_fontawesome_1.FontAwesomeIcon, { icon: pressed ? free_solid_svg_icons_1.faLockOpen : free_solid_svg_icons_1.faLock, size: 24, color: _this.context.fontColor }))));
                        })) : (react_1["default"].createElement(react_native_1.Text, { style: {
                                color: this.context.fontColor,
                                fontSize: 16,
                                fontWeight: "bold",
                                paddingLeft: 10
                            } }, global.strings.Access1 +
                            this.context.activeCompany.domain +
                            global.strings.Access2)))))),
                react_1["default"].createElement(components_1.Modal, { visible: currentSerie.loading, style: {
                        alignItems: "center",
                        justifyContent: "center",
                        backgroundColor: Colors_1["default"].ModalBackground
                    } },
                    react_1["default"].createElement(react_native_indicators_1.PulseIndicator, { size: 50, color: this.context.activeCompany.font_color || Colors_1["default"].Text1 }))));
        }
        else {
            return react_1["default"].createElement(common_1.Background, { empty: true });
        }
    };
    SerieView.contextType = CompanyProvider_1.ActiveCompanyContext;
    return SerieView;
}(react_1["default"].Component));
exports.SerieView = SerieView;
var styles = react_native_1.StyleSheet.create({
    flex: {
        flex: 1
    },
    livePreviewStyle: {
        height: 163,
        width: 290,
        borderRadius: 5,
        backgroundColor: "black"
    },
    liveButtonStyle: {
        flex: 1,
        justifyContent: "flex-end"
    },
    liveDataContainerStyle: {
        flexWrap: "wrap",
        elevation: 1,
        marginLeft: 8,
        marginRight: 8
    },
    liveDetailsContainerStyle: {
        marginRight: 4,
        justifyContent: "center"
    },
    liveTitleStyle: {
        fontWeight: "bold",
        fontSize: 15,
        marginTop: 4,
        marginLeft: 10,
        opacity: 0.7
    },
    liveCollectionStyle: {
        fontWeight: "700",
        padding: 4,
        backgroundColor: "rgb(48,48,48)",
        alignSelf: "flex-end",
        marginTop: 3,
        marginRight: 3,
        fontSize: 14,
        marginBottom: 10,
        color: "white"
    },
    liveDateStyle: {
        color: Colors_1["default"].PlaceholderText,
        marginBottom: 2,
        fontSize: 14
    },
    listFooterStyle: {
        height: 80
    },
    noLiveTextStyle: {
        color: Colors_1["default"].Text1,
        marginLeft: 3,
        fontSize: 16
    },
    spinnerContainerStyle: {
        justifyContent: "center",
        alignItems: "center"
    },
    boxTitleStyle: {
        alignItems: "center",
        flexDirection: "row",
        backgroundColor: "rgb(48,48,48)",
        justifyContent: "center"
    },
    // footer
    buttonIconContainerStyle: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    buttonGradientStyle: {
        height: 80,
        width: 80,
        alignSelf: "center",
        borderRadius: 50,
        elevation: 1
    },
    buttonIconStyle: {
        fontWeight: "600",
        alignSelf: "center"
    },
    buttonStyle: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    translucentStyle: {
        flex: 1,
        justifyContent: "flex-end",
        borderRadius: 5,
        padding: 8
    },
    imgStyle: {
        height: 50,
        width: 50,
        alignSelf: "center",
        margin: 5
    },
    // modal
    modalWindowContainer: {
        alignItems: "center",
        justifyContent: "center"
    },
    modalWindowStyle: {
        backgroundColor: Colors_1["default"].Text1,
        borderColor: Colors_1["default"].Border1,
        borderRadius: 7,
        elevation: 1
    },
    modalQuestionContainerStyle: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: "center",
        justifyContent: "center"
    },
    modalQuestionStyle: {
        fontSize: 18
    },
    modalAnswerContainerStyle: {
        alignSelf: "flex-end",
        flexDirection: "row"
    },
    modalAnswerTextStyle: {
        borderColor: Colors_1["default"].Border1,
        width: 150,
        height: 52,
        alignItems: "center",
        justifyContent: "center"
    },
    paymentInput: {
        width: 150,
        height: 40,
        color: Colors_1["default"].Text1,
        // backgroundColor: 'red',
        fontSize: 18,
        marginTop: react_native_1.Platform.OS == "ios" ? 8 : 0
    },
    trailerTitleStyle: {
        textAlignVertical: "center",
        color: "white",
        padding: 7,
        fontWeight: "bold",
        fontSize: 17,
        marginRight: 10,
        textAlign: "center"
    },
    iconaPlayStyle: {
        textAlign: "center",
        marginLeft: 10,
        opacity: 0.8
    }
});
