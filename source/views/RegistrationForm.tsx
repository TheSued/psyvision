import React from "react";
import { View, Image, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import CheckBox from "react-native-check-box";
import Storage from "../services/Storage";
import {
  OrientationLocker,
  PORTRAIT,
  LANDSCAPE,
} from "react-native-orientation-locker";
import * as Network from "../network";
import { ActiveCompanyContext } from "../providers/CompanyProvider";
import {
  Background,
  AppText,
  Input,
  Expand,
  FadeButton,
} from "../components/common";

interface RegistProps {
  navigation: any;
}
class RegistrationForm extends React.PureComponent<RegistProps> {
  static contextType = ActiveCompanyContext;

  state = {
    email: "",
    password: "",
    repeatPassword: "",
    termsAgreed: false,
    privacyAgreed: false,
    promotionsAgreed: false,
    message: "",
    registering: false,
    showError: false,
    pageHasFocus: false,
    isLogging: false,
  };

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", () =>
      this.setState({ pageHasFocus: true })
    );
    this.blurListener = this.props.navigation.addListener("blur", () =>
      this.setState({ pageHasFocus: false })
    );
  }

  openTerms() {
    this.props.navigation.navigate("WebViewPage", {
      source: this.context.activeCompany.terms,
    });
  }

  openPrivacyPolicy() {
    this.props.navigation.navigate("WebViewPage", {
      source: this.context.activeCompany.privacy_policy,
    });
  }

  onError(message: string) {
    this.setState({ message: message }, () =>
      setTimeout(() => this.setState({ showError: true }), 500)
    );
  }

  isFormValid(): boolean {
    const { email, password, repeatPassword, termsAgreed } = this.state;

    if (email && password && repeatPassword) {
      if (termsAgreed) {
        if (password === repeatPassword && password.length >= 8) {
          return true;
        } else {
          if (password.length < 8) {
            this.onError(global.strings.passwordLengthError);
          } else {
            this.onError(global.strings.MismatchingPasswords);
          }
        }
      } else {
        this.onError(global.strings.TermsUnchecked);
      }
    } else {
      this.onError(global.strings.MissingFormFields);
    }
    return false;
  }

  onSubmit() {
    const { email, password, repeatPassword, privacyAgreed, promotionsAgreed } =
      this.state;
    this.setState({ showError: false, message: "" }, async () => {
      if (this.isFormValid()) {
        try {
          const registrationResult = await Network.registerUser(
            email,
            password,
            repeatPassword,
            privacyAgreed,
            promotionsAgreed
          );
          console.log(registrationResult.status);
          if (registrationResult.status !== "error") {
            if (registrationResult.token) {
              await Storage.saveToken(registrationResult.token);
              this.setState({ message: "", showError: false });
              this.onError(global.strings.RegistrationSuccessfull);
              this.context.signedOut();
            } else {
              this.onError(global.strings.GeneralError);
            }
          } else {
            if (registrationResult.type === "email_already_registerd") {
              this.onError(global.strings.emailAlreadyRegisterd);
            } else if (
              registrationResult.type === "registration_not_allowed" ||
              registrationResult.type === "companies_not_exist"
            ) {
              this.onError(global.strings.GeneralError);
            } else {
              this.onError(global.strings.RegistrationError);
            }
          }
        } catch (error) {
          console.log(error);
        }
      }
    });
  }

  render() {
    const { privacy_policy, terms } = this.context.activeCompany;
    if (this.state.pageHasFocus) {
      return (
        <Background>
          <View style={styles.headerView}>
            <OrientationLocker orientation={PORTRAIT} />
            <Icon
              name="arrow-back"
              size={30}
              color={this.context.fontColor}
              onPress={() => this.props.navigation.goBack()}
            />
          </View>
          <View style={styles.backgroundStyle}>
            <Image
              resizeMode={"contain"}
              source={{ uri: `${this.context.activeCompany.logo}` }}
              style={styles.imageStyle}
            />

            {/* Error message */}
            <Expand
              params={{ start: 0, end: 60 }}
              controller={this.state.message !== ""}
              style={{ justifyContent: "center" }}
            >
              <AppText style={[styles.errorTextStyle,{ color: this.context.fontColor}]}>
                {this.state.showError ? this.state.message : ""}
              </AppText>
            </Expand>

            <View style={[styles.formContainerFirst,{backgroundColor: this.context.alphaColor}]}>
              <Input
                autoCapitalize={"none"}
                autoCorrect={false}
                onChangeText={(text: string) => this.setState({ email: text })}
                value={this.state.email}
                placeholder={global.strings.EmailPlaceholder}
                style={[styles.formInput,{color: this.context.fontColor}]}
              />
            </View>

            <View style={[styles.formContainer,{backgroundColor: this.context.alphaColor}]}>
              <Input
                onChangeText={(text: string) =>
                  this.setState({ password: text })
                }
                value={this.state.password}
                placeholder={global.strings.PasswordPlaceholder}
                secureTextEntry
                style={styles.formInput}
              />
            </View>
            <View style={[styles.formContainerLast,{backgroundColor: this.context.alphaColor}]}>
              <Input
                onChangeText={(text: string) =>
                  this.setState({ repeatPassword: text })
                }
                value={this.state.repeatPassword}
                placeholder={global.strings.RepeatPasswordPlaceholder}
                secureTextEntry
                style={styles.formInput}
              />
            </View>

            {terms && (
              <View style={[styles.checkboxContainer, { marginTop: 20 }]}>
                <CheckBox
                  checkBoxColor={this.context.fontColor}
                  style={{ justifyContent: "center" }}
                  onClick={() => {
                    this.setState({ termsAgreed: !this.state.termsAgreed });
                  }}
                  isChecked={this.state.termsAgreed}
                />
                <AppText
                  style={[styles.agreementStyle,{color:this.context.fontColor}]}
                  onPress={() => this.openTerms()}
                >
                  {global.strings.TermsAgreement}
                  <AppText style={[styles.agreementStyleLink,{ color: this.context.fontColor}]}>
                    {global.strings.Terms}
                  </AppText>
                </AppText>
              </View>
            )}

            {privacy_policy && (
              <View style={[styles.checkboxContainer, { marginBottom: 40 }]}>
                <CheckBox
                  checkBoxColor={this.context.fontColor}
                  style={{ justifyContent: "center" }}
                  onClick={() => {
                    this.setState({ privacyAgreed: !this.state.privacyAgreed });
                  }}
                  isChecked={this.state.privacyAgreed}
                />
                <AppText
                  style={[styles.agreementStyle,{color: this.context.fontColor}]}
                  onPress={() => this.openPrivacyPolicy()}
                >
                  {global.strings.PrivacyAgreement}
                  <AppText style={styles.agreementStyleLink}>
                    {global.strings.PrivacyPolicy}
                  </AppText>
                </AppText>
              </View>
            )}
            {/* 
            <View style={styles.checkboxContainer}>
              <CheckBox
                checkBoxColor={Colors.Text1}
                style={{ justifyContent: 'center' }}
                onClick={() => {
                  this.setState({
                    promotionsAgreed: !this.state.promotionsAgreed,
                  });
                }}
                isChecked={this.state.promotionsAgreed}
              />
              <AppText style={styles.agreementStyle}>
                {global.strings.PromotionAgreement}
              </AppText>
            </View> */}

            <FadeButton
              style={styles.buttonContainer}
              textStyle={[styles.buttonText,{color: this.context.fontColor}]}
              alphaValue={[1, 0.1]}
              controller={this.state.registering}
              onPress={() => this.onSubmit()}
            >
              {global.strings.SignUp}
            </FadeButton>
          </View>
        </Background>
      );
    } else {
      return <Background empty />;
    }
  }
}

const styles = StyleSheet.create({
  headerView: {
    padding: 8,
  },

  flex: {
    flex: 1,
  },

  // logo
  backgroundStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  imageStyle: {
    height: 107,
    width: 190,
    alignSelf: "center",
    marginBottom: 24,
  },

  // error text
  errorTextStyle: {
   
    fontSize: 17,
    elevation: 1,
  },

  buttonContainer: {
    borderWidth: 1,
    borderStyle: "solid",
    elevation: 1,
    width: 300,
    backgroundColor: "transparent",
  },
  buttonText: {
    alignSelf: "center",
    fontSize: 22,
    paddingTop: 12,
    paddingBottom: 12,
    elevation: 1,
  },
  formContainerFirst: {
    width: 300,
    marginTop: 13,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  formContainer: {
    width: 300,
    marginTop: 2,
  },
  formContainerLast: {
    width: 300,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    marginTop: 2,
  },
  formInput: {
    fontSize: 20,

    height: 60,
    paddingLeft: 15,
  },
  logoStyle: {
    height: 134,
    width: 200,
  },
  agreementStyle: {
    fontSize: 16,
    marginVertical: 4,
    paddingHorizontal: 6,
  },
  agreementStyleLink: {
    textDecorationLine: "underline",
  },
  checkboxContainer: {
    width: 300,
    flexDirection: "row",

    marginLeft: 20,
  },
});

export { RegistrationForm };
