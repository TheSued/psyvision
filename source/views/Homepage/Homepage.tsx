import React from "react";

import {
  OrientationLocker,
  PORTRAIT,
  LANDSCAPE,
} from "react-native-orientation-locker";
import {
  View,
  ImageBackground,
  Linking,
  TouchableWithoutFeedback,
  Text,
} from "react-native";

// import type { Notification, NotificationOpen } from "react-native-firebase";
import { PulseIndicator } from "react-native-indicators";

import { ActiveCompanyContext } from "../../providers/CompanyProvider";
import {
  Header,
  CatalogList,
  SerieList,
  Modal,
  Toast,
  HomePageList,
  VideoPlayer,
} from "../../components";
import {
  Background,
  AppText,
  Slideshow,
  Touchable,
} from "../../components/common";

import * as Network from "../../network";

import Colors from "../../environments/Colors";
import { formatVideoObject } from "../../utils/common.utils";
import * as HomepageUtils from "./Homepage.utils";
import { ScrollView } from "react-native-gesture-handler";
import Pusher from "pusher-js/react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { getSerieInfo } from "../../network";
import { color } from "react-native-reanimated";
import Strings_en_GB from "../../environments/languages/Strings-en_GB";
import Strings_it_IT from "../../environments/languages/Strings-it_IT";
import Strings_es_ES from "../../environments/languages/Strings-es_ES";
import Strings_fr_FR from "../../environments/languages/Strings-fr_FR";
import { AppCompanyId, isCustomApp } from "../../environments/AppEnvironment";
import { getUserData } from "../../network/user";

const currentScreen = "Homepage";

interface HomepageProps {
  navigation: any;
  route: any;
}

class Homepage extends React.Component<HomepageProps> {
  static contextType = ActiveCompanyContext;
  private focusListener: any;
  private blurListener: any;
  private notificationOpenedListener: any;
  private socket: any;
  private sessionChannel: any;
  private searchText: string;
  private searchCategories: any[] = [];

  state = {
    content: [],

    requesting: true,
    userBoughtBundle: false,
    changeCompanyModal: false,

    // search states
    search: "",
    searchDataSeries: {
      data: [],
      step: 1,
    },
    searchDataVideos: {
      data: [],
      step: 1,
    },
    pageHasFocus: false,
    quizAlert: false,
    loadingInfoVideo: false,
    errorMessage: "",
    searchLoading: false,
    selectedSerie: null,

    modalVisible: false,

    currentSerie: {
      id: null,
      data: [],
      step: 1,
      loading: false,
    },

    isTitleClicked: false,
  };

  async notificationCheck() {
    // try {
    //   const fcmToken = await firebase.messaging().getToken();
    //   const permissionEnabled = await firebase.messaging().hasPermission();
    //   if (!permissionEnabled) {
    //     await firebase.messaging().requestPermission();
    //   }
    //   this.setupNotifications(fcmToken);
    // } catch (error) {}
  }
  loadingMap = new Map();
  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", () =>
      this.viewWillFocus()
    );
    this.blurListener = this.props.navigation.addListener("blur", () =>
      this.viewWillBlur()
    );
    const { params } = this.props.route;
    // Notification Setup
    // this.notificationCheck();
    // app opened by notification
    if (params && params.video) {
      this.openPlayer({ id: params.video });
    }
    this.requests();
    this.changeUserLanguage();
  }
  changeUserLanguage = async () => {
    const companyId = isCustomApp() && AppCompanyId;
    const data = await getUserData(companyId);

    if (data.language === "en") {
      global.strings = Strings_en_GB;
      this.context.setCompanySettingsLanguage("en");
    } else if (data.language === "it") {
      global.strings = Strings_it_IT;
      this.context.setCompanySettingsLanguage("it");
    } else if (data.language === "es") {
      global.strings = Strings_es_ES;
      this.context.setCompanySettingsLanguage("es");
    } else if (data.language === "fr") {
      global.strings = Strings_fr_FR;
      this.context.setCompanySettingsLanguage("fr");
    }
  };

  // setupNotifications(fcmToken: string) {
  //   Network.setDeviceToken(fcmToken);
  //   this.notificationOpenedListener = firebase
  //     .notifications()
  //     .onNotificationOpened((notificationOpen: NotificationOpen) => {
  //       const notification: Notification = notificationOpen.notification;
  //       const shouldOpenPlayer =
  //         ["video", "video_live", "video_upload"].indexOf(
  //           notification._data.type
  //         ) !== -1;
  //       if (shouldOpenPlayer) {
  //         this.openPlayer({ id: parseInt(notification._data.id) });
  //       }
  //     });
  // }

  async setSessionListener() {
    if (!this.socket) {
      this.socket = new Pusher("1951c674aef8e8b86430", {
        cluster: "eu",
      });
    }
    const token = await Network.getToken();
    if (token) {
      if (!this.sessionChannel) {
        this.sessionChannel = this.socket.subscribe("notification_" + token);
        this.sessionChannel.bind("notification", (data) => {
          let dataParse = JSON.parse(data);
          if (dataParse.type === "session_ended") {
            this.context.signedOut();
          }
        });
      }
    }
  }

  viewWillFocus() {
    // reset nested serie if the user change the company while on a nested SerieView
    // or click on the logo to go back on the catalog
    this.setSessionListener();
    this.setState({ pageHasFocus: true });
    if (this.state.selectedSerie) {
      this.openSeries(this.state.selectedSerie);
    }
    if (this.context.paymentSuccessfull) {
      this.context.togglePaymentSuccessfull();
    }
  }

  viewWillBlur() {
    this.setState({ pageHasFocus: false });
  }

  componentWillUnmount() {
    if (this.socket && this.sessionChannel) {
      this.sessionChannel.unbind_global();
      this.socket.unsubscribe();
    }
    this.focusListener();
    this.blurListener();
  }

  onError(error: string) {
    this.setState({ errorMessage: error }, () => {
      setTimeout(() => this.setState({ errorMessage: "" }), 1800);
    });
  }

  async requests() {
    const { activeCompany, user } = this.context;

    const homepageSettings = await Network.getHomepageSettings(
      activeCompany.id,
      !!user
    );
    this.context.updateContent(homepageSettings.content);

    // map values
    const parsedContent = await homepageSettings.content
      .filter((el: any) => {
        return el != null && el.type != "custom";
      })
      .map((item: any) => {
        if (item.val.id_cat) {
          item.val.id = item.val.id_cat[0];
        }
        item.val.title =
          item.val.title_section || item.val.title_cat || item.val.title;
        return item;
      });

    // filter app valid content to render
    const filteredContent = parsedContent.filter(
      (item) =>
        HomepageUtils.needData(item.type) || item.type === "header_section"
    );

    // populate requests
    const promises: any[] = [];
    for (const el of filteredContent) {
      const functionForItem = HomepageUtils.getFunctionForItem(
        activeCompany.id,
        el.val.id,
        el.type,
        1
      );
      if (functionForItem) {
        promises.push(functionForItem);
      }
    }
    // populate content from requests
    const requestData = await Promise.all(promises);
    for (const el of filteredContent) {
      if (HomepageUtils.needData(el.type)) {
        el.data = HomepageUtils.mapData(el.type, requestData[0]);
        el.step = 1;
        requestData.splice(0, 1);
      }
    }

    this.setState({ content: filteredContent, requesting: false });
  }

  async fetchMore(index: number) {
    if (this.loadingMap.get(index) === true) {
      return false;
    }
    this.loadingMap.set(index, true);
    //prendo dati gia esistenti all'interno dello stato
    const item = JSON.parse(JSON.stringify(this.state.content[index]));
    const { activeCompany } = this.context;

    if (item.data.length && item.data.length % global.videoEachStep === 0) {
      const functionForItem = await HomepageUtils.getFunctionForItem(
        activeCompany.id,
        item.val.id,
        item.type,
        item.step ? item.step + 1 : 2
      );
      //inizializzo uno step che verrà successivamente modificato per essere usato come metro di valutazione per le successive call
      let step = 0;
      //se ho ricevuto risposta dal server PRESSOCHé inutile
      if (functionForItem) {
        const data = functionForItem;
        if (data.length === global.videoEachStep) {
          step = item.step ? item.step + 1 : 2;
        } else {
          step = item.step;
        }

        const content = JSON.parse(JSON.stringify(this.state.content));
        content[index].data = [].concat(content[index].data, data);
        content[index].step = step;
        this.setState({ content }, () => {
          this.loadingMap.set(index, false);
        });
      }
    }
  }

  async openSeriesTitle(serieId) {
    this.setState({
      currentSerie: { ...this.state.currentSerie, loading: true },
    });
    const data = await Network.getSerieInfo(serieId);
    let serie;
    if (serieId.parent) {
      serie = await Network.getSerieInfo(serieId.id_child);
    } else {
      serie = await Network.getSerieInfo(serieId.id_parent);
    }
    this.setState({
      currentSerie: { ...this.state.currentSerie, loading: false },
    });
    if (data && data[0]) {
      this.props.navigation.push("SerieView", {
        serie: data[0],
        previousScreen: currentScreen,
      });
    }
  }

  openLink(externalUrl) {
    this.props.navigation.navigate("WebViewPage", {
      source: externalUrl,
    });
  }

  openSeries(serie) {
    this.context.setActualPreview(serie.img_preview);
    this.props.navigation.navigate("SerieView", {
      serie: serie,
      previousScreen: currentScreen,
    });
  }

  openElement(element: any) {
    if (element.groupType === "video") {
      this.openPlayer(element);
    } else {
      this.openSeries(element);
    }
  }

  async openPlayer(item: any) {
    if (item.quiz) {
      this.setState({ quizAlert: true });
    } else {
      this.context.setActualPreview(item.img_preview);
      this.setState({ loadingInfoVideo: true });
      const promises = await Promise.all([
        Network.getVideoInfo(item.id), // needed in case we open a video from notification
        Network.getVideoSource(item.id),
        Network.getAttachments(item.id),
      ]);
      const videoInfo = promises[0];
      const data = promises[1];
      const attachments = promises[2];

      if (data && data.status === "success") {
        let video = formatVideoObject(data);
        if (!videoInfo.ondemand) {
          video.started = videoInfo.livenow;
        }
        this.props.navigation.navigate("Player", {
          previousScreen: currentScreen,
          video: {
            id: item.id,
            creator: item.display_name
              ? item.display_name
              : videoInfo.display_name, // ? needed in case we open a video from notification
            title: item.title ? item.title : videoInfo.title, // ? needed in case we open a video from notification
            description: item.description
              ? item.description
              : videoInfo.description, // ? needed in case we open a video from notification
            source: video,
            sourceChromecast: data.source_chromecast,
            sourceMP4: data.source_original,
            attachments: attachments,
          },
        });
      } else if (
        data.status == "error_access_not_permit" &&
        data.id_series &&
        data.id_series != ""
      ) {
        // user need to pay the serie containing the video
        const serieData = await Network.getSerieInfo(data.id_series);
        this.setState({ loadingInfoVideo: false }, () =>
          this.props.navigation.navigate("SerieView", {
            serie: serieData[0],
            previousScreen: currentScreen,
          })
        );
      } else {
        this.onError(global.strings.NoVideoAccess);
      }
      if (this.state.loadingInfoVideo) {
        // needed in case we are in SerieView already
        this.setState({ loadingInfoVideo: false });
      }
    }
  }

  async onTitlePress(item) {
    if (item.val.id === "live_scheduled") return null;
    if (item.val && item.val.id_cat) {
      this.props.navigation.navigate("Searchpage", {
        id_cat: item.val.id_cat,
        previousScreen: currentScreen,
      });
    } else if (item.type && item.type == "list_video") {
      const serieId = item.val.id;
      this.openSeriesTitle(serieId);
    }
  }

  renderLists() {
    const { content, modalVisible } = this.state;
    return content.map((item: any, index: number) => {
      switch (item.type) {
        case "header_section":
          let call = item.val.callToAction
            ? item.val.callToAction.split("-")
            : false;
          let baseDomain = this.context.activeCompany.domain;
          let externalUrl = `${baseDomain}/app/channelPage?${call[0]}=${call[1]}`;

          return (
            <ImageBackground
              key={index}
              resizeMode={"cover"}
              source={{ uri: item.val.background }}
              style={{ width: "100%", height: 200, marginBottom: 28,  }}
            >
              <ScrollView
                style={{
                  width: "100%",
                  height: "100%",
                  opacity: 1,
                  backgroundColor: "rgba(0,0,0,0.35)"
                }}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  justifyContent: "center",
                  paddingHorizontal: "10%",
                  paddingVertical: "5%",
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: "700",
                    lineHeight: 23,
                    elevation: 1,
                    marginLeft: 6,
                    color: this.context.buttonTextColor,
                  }}
                >
                  {item.val.title}
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "600",
                    lineHeight: 23,
                    elevation: 1,
                    marginLeft: 6,
                    color: this.context.buttonTextColor,

                  }}
                >
                  {item.val.description}
                </Text>

                {call[0] === "serie" && (
                  <View
                    style={{
                      flexDirection: "row",
                      flexWrap: "wrap",
                      marginLeft: 6,
                    }}
                  >
                    <Touchable
                      style={{
                        backgroundColor: this.context.buttonColor,
                        alignItems: "center",
                        padding: 10,
                        borderRadius: 5,
                        flexDirection: "row",
                        flexWrap: "wrap",
                      }}
                      onPress={() => this.openSeriesTitle(call[1])}
                    >
                      <AppText style={{ color: this.context.fontColor }}>
                        {item.val.callToActionBtn}
                      </AppText>
                    </Touchable>
                  </View>
                )}

                {(call[0] === "page" || call[0] === "link") && (
                  <View
                    style={{
                      flexDirection: "row",
                      flexWrap: "wrap",
                      marginLeft: 6,
                    }}
                  >
                    <Touchable
                      style={{
                        backgroundColor: this.context.buttonColor,
                        alignItems: "center",
                        padding: 10,
                        borderRadius: 5,
                        flexDirection: "row",
                        flexWrap: "wrap",
                      }}
                      onPress={() => this.openLink(externalUrl)}
                    >
                      <AppText style={{ color: this.context.fontColor }}>
                        {item.val.callToActionBtn}
                      </AppText>
                    </Touchable>
                  </View>
                )}
              </ScrollView>
            </ImageBackground>
          );
        case "description_video":
          return (
            <>
              <AppText
                style={{
                  fontSize: 18,
                  fontWeight: "700",
                  lineHeight: 23,
                  elevation: 1,
                  marginLeft: 6,
                }}
              >
                {item.val.title}
              </AppText>
              <AppText style={{ marginLeft: 6, marginBottom: 10 }}>
                {item.val.description}
              </AppText>
              <VideoPlayer
                id={+item.val.id}
                title={item.val.title}
                uri={item.data.source}
                live={false}
                started={true}
                fullscreen={false}
                ref={(ref) => {
                  this.videoPlayer = ref;
                }}
              />
              <View style={{ height: 28 }} />
            </>
          );
        case "header_slideshow_serie":
        case "header_slideshow_pack":
          return (
            <Slideshow
              key={index}
              //title={item.val.title}
              data={item.data}
              onItemPress={this.openElement.bind(this)}
            />
          );
        case "list_pack":
        case "list_serie":
        case "list_video":
          return (
            <HomePageList
              horizontal={true}
              isFetching={this.state.isFetching}
              key={index}
              onTitlePress={() => this.onTitlePress(item)}
              title={item.val.title}
              id={item.val.id}
              data={item.data}
              defaultPreview={this.context.activeCompany.default_video_image}
              onRef={(ref) => (this.onItemPress = ref)}
              onItemPress={this.openElement.bind(this)}
              onEndReachedThreshold={10}
              onEndReached={() => this.fetchMore(index)}
            />
          );
        default:
          return null;
      }
    });
  }

  closeCompanyModal() {
    this.setState({ changeCompanyModal: false });
  }

  onLogoPress() {
    if (this.context.companies.length > 1 && !isCustomApp()) {
      this.setState({ changeCompanyModal: true });
    }
  }

  render() {
    if (this.state.pageHasFocus) {
      const {
        quizAlert,
        loadingInfoVideo,
        changeCompanyModal,
        searchLoading,
        requesting,

        currentSerie,
      } = this.state;

      return (
        <View style={{ flex: 1 }}>
          <OrientationLocker orientation={PORTRAIT} />
          <Background
            header={
              <Header
                home
                onLogoPress={() => this.onLogoPress()}
                onMenuPress={() => {
                  if (this.context.user) {
                    this.props.navigation.navigate("Profile", {
                      previousScreen: currentScreen,
                    });
                  } else {
                    this.props.navigation.navigate("Login", {
                      previousScreen: currentScreen,
                    });
                  }
                }}
                onSearchPress={() =>
                  this.props.navigation.navigate("Searchpage", {
                    previousScreen: currentScreen,
                  })
                }
                onCameraPress={() =>
                  this.props.navigation.navigate("ManageLive", {
                    previousScreen: currentScreen,
                  })
                }
              />
            }
          >
            {!this.state.isSearching && (
              <View style={{}}>{this.renderLists()}</View>
            )}

            {/* search lists */}
            {this.state.isSearching && (
              <View>
                {searchLoading && (
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center",
                      justifyContent: "center",
                      backgroundColor: this.context.backgroundColor,
                    }}
                  >
                    <PulseIndicator size={50} color={this.context.fontColor} />
                  </View>
                )}
                {!searchLoading && (
                  <View>
                    <SerieList
                      label={global.strings.Series}
                      data={this.state.searchDataSeries.data}
                      onRef={(ref) => (this.onItemPress = ref)}
                      onItemPress={this.openSeries.bind(this)}
                    />

                    <CatalogList
                      defaultPreview={
                        this.context.activeCompany.default_video_image
                      }
                      label={"Video"}
                      data={this.state.searchDataVideos.data}
                      onRef={(ref) => (this.onItemPress = ref)}
                      onItemPress={this.openPlayer.bind(this)}
                    />
                  </View>
                )}
              </View>
            )}
          </Background>
          <Toast message={this.state.errorMessage} />

          {/* modal */}
          <Modal
            visible={
              quizAlert ||
              loadingInfoVideo ||
              changeCompanyModal ||
              requesting ||
              currentSerie.loading
            }
            style={{
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: this.context.backgroundColor,
            }}
          >
            {(loadingInfoVideo || requesting || currentSerie.loading) && (
              <PulseIndicator size={50} color={this.context.fontColor} />
            )}
            {quizAlert && (
              <View
                style={{
                  marginRight: 16,
                  marginLeft: 16,
                  backgroundColor: this.context.backgroundColor,
                  elevation: 1,
                  borderRadius: 8,
                  width: "90%",
                }}
              >
                <View
                  style={{ alignItems: "center", justifyContent: "center" }}
                >
                  <AppText
                    style={{
                      fontSize: 16,
                      padding: 16,
                      paddingBottom: 0,
                      color: this.context.fontColor,
                    }}
                  >
                    {global.strings.QuizAlert}
                  </AppText>
                </View>
                <Touchable
                  style={{ alignItems: "center", justifyContent: "center" }}
                  onPress={() => this.setState({ quizAlert: false })}
                >
                  <AppText
                    style={{
                      fontSize: 16,
                      padding: 8,
                      paddingBottom: 16,
                      color: this.context.fontColor,
                    }}
                  >
                    {global.strings.Ok}
                  </AppText>
                </Touchable>
              </View>
            )}
          </Modal>
        </View>
      );
    } else {
      return <Background empty />;
    }
  }
}

export { Homepage };
